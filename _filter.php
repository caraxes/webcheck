<?php
/**
 * filter class
 * @author �ukasz Kaczmarek <caraxes@gmail.com>
 * @copyright 2010 by CRX, http://scan.webcrx.com/
 * @licence GPU
 *
 */
class filter {
	/**
	*
	* filtruje dane i zwraca oczekiwana zmienna
	*
	* @param string $var nazwa zmiennej
	* @param string $expected typ oczekiwanej zmiennej
	*
	* @return string
	**/
	public function filterVar($var,$expected) {
		switch($expected){
			case 'int':
				return intval($var);
			break;
			case 'double'://, 'float', 'real':
				return floatval($var);
			break;
			case 'string':
				return strval($var);
			break;
			default:
				return '';
			break;
		}
	}
	
	/**
	*
	* sprawdza czy wprowadzona zmienna jest tym za czym sie podaje
	*
	* @param string $var nazwa zmiennej
	* @param string $expected typ oczekiwanej zmiennej
	*
	* @return bool
	**/
	public function checkVar($var,$expected) {
		switch($expected){
			case 'numeric':
				if(is_numeric($var)) return true;
				else return false;
			break;
			case 'string':
				if(is_string($var)) return true;
				else return false;
			break;
			case 'array':
				if(is_array($var)) return true;
				else return false;
			break;
			case 'object':
				if(is_object($var)) return true;
				else return false;
			break;
			case 'float':
				if(is_float($var)) return true;
				else return false;
			break;
			case 'bool':
					$strict=false;
					$out = null;
					if (in_array($var,array('false', 'False', 'FALSE', 'no', 'No', 'n', 'N', '0', 'off',
										   'Off', 'OFF', false, 0, null), true)) {
						$out = false;
					} else if ($strict) {
						if (in_array($var,array('true', 'True', 'TRUE', 'yes', 'Yes', 'y', 'Y', '1',
											   'on', 'On', 'ON', true, 1), true)) {
							$out = true;
						}
					} else {
						$out = ($var?true:false);
					}
					return $out;
				
			break;
			case 'null':
				if(!isset($var)) return true;
				else return false;
			break;
			default:
				return '';
			break;
		}
	}
	
	/**
	*
	* filtruje dane i zamienia znaki specjalne na HTML Entity i ISO Latin-1 code
	*
	* @param string $var nazwa zmiennej do sprawdzenia
	*
	* @return bool and if return true return string
	**/
	public function cleanVar($var) {
		return str_replace(array(';', '&', '"', "'", '`', '<', '>', '\ ', '/', '(', ')'), array("&x3B;", "&amp;", "&quot;", "&#x27;", "&#x60;", "&lt;", "&gt;", "&#92;", "&frasl;", "&#40;", "&#41;"), $var);
	}
	
	/**
	*
	* filtruje dane i usuwa znaki specjalne
	*
	* @param string $var nazwa zmiennej do sprawdzenia
	*
	* @return bool and if return true return string
	**/
	public function removeVar($var) {
		return str_replace(array(';', '&', '"', "'", '`', '<', '>', 'script', '\ ', '/', '(', ')'), array("", "", "", "", "", "", "", "", "", "", "", ""), $var);
	}

}

$filter = new filter();

//PRZYK�ADY
echo $filter->filterVar('test123','string'); // zwr�ci 'test'
echo $filter->filterVar('test123','int'); // zwr�ci '123'
echo $filter->checkVar('test123', 'string'); // zwr�ci FALSE
echo $filter->checkVar('test', 'string'); // zwr�ci TRUE
echo $filter->checkVar('1', 'bool'); // zwr�ci TRUE
echo $filter->cleanVar('aaa<script> alert </script>', 'string'); // zwr�ci - aaa&lt;script&gt; alert &lt;&frasl;script&gt;
echo $filter->removeVar('aaa<script> alert </script>'); // zwr�ci - 'aaa alert'

?>
