<?php
/**********
author : Łukasz 'caraxes' K. &copy; 2009
**********/
	class sub {
	 
	  protected $host;
	  protected $user;
	  protected $pwd;
	  protected $dbName;
	 
		 function __construct($host, $user, $pwd, $dbName){
			$this->host = $host;
			$this->user = $user;
			$this->pwd = $pwd;
			$this->dbName = $dbName;
		}
		
		public function sub_add() {
			try 
			{
			$pdo = new PDO('mysql:host='.$this->host.';dbname='.$this->dbName.'', ''.$this->user.'', ''.$this->pwd.'', array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
			
				if($_SERVER['REQUEST_METHOD'] == 'POST') 
				{ 	
					try
				   {
					  

					//DATA VERIFICATION:  
					  
					$formval = new formValidator();
					

						$formval -> validateWWW('www','Proszę podać poprawny adres strony www',3,500);
						$formval -> validateEmail('email','Prosze podać poprawny email');					
						
						$formval -> validateWWWexist('www',$pdo,'Podana strona www jest już w bazie, prosze podać inna');

						$formval -> validateCaptcha('cap','Proszę podać wynik z obrazka !');
						
						$formval -> validateEmpty('ok','Proszę zatwierdzić regulamin',1,1);
						
						$formval_errors_number = $formval -> checkErrors();
							if($formval_errors_number > 0)
								echo $formval -> displayErrors();
							
					//DATA VERIFICATION end:  
					
					$host = gethostbyaddr($_SERVER['REMOTE_ADDR']);
					$ip = $_SERVER['REMOTE_ADDR'];
					
					  $sql = $pdo -> prepare("INSERT INTO `data_to_verify` (`imie`, `www`, `email`, `uwagi`, ip, host, data_dodania) VALUES
								(
								:imie,
								:www,
								:email,
								:uwagi,
								'".$ip."',
								'".$host."',
								NOW());");
								
					  $sql -> bindParam(':imie', $_POST['imie'], PDO::PARAM_STR, 50);
					  $sql -> bindParam(':www', $_POST['www'], PDO::PARAM_STR, 500);
					  $sql -> bindParam(':email', $_POST['email'], PDO::PARAM_STR, 100);
					  $sql -> bindParam(':uwagi', $_POST['comment'], PDO::PARAM_STR, 500);
					
					   if($formval_errors_number == 0) {
							$sql -> execute();
							//print_r($sql->errorInfo());
							$sql->closeCursor();
							
							$email=new sendMail(); 
							$email->html();
							$email->from('noreply@webcheck.pl', 'CRX Web Check'); 
							$email->add_recipient($_POST['email']);//add a recipient in the to: field 
							//$email->add_cc('person2@example.com');//carbon copy 
							//$email->add_bcc('person3@example.com');//blind carbon copy 
							$email->subject('Twoja strona została dodana do bazy danych serwera crx web check');//set subject 
							$email->message('Dziękujemy za dodanie danych do bazy, w niedługim czasie postaram się skontaktować i okazać wyniki skanowania! Proszę pamiętać, że akrypt jest nadal w fazie testowania, pozdrawiam');//set message body 
							$email->send();//send email(s)
							
							unset($_POST);
							echo 'Dane dodane, dziękujemy ! Na podany adres został wysłany email z instrukcją';
					   }
				   }
				   catch(PDOException $e)
				   {
					  echo 'Połączenie nie mogło zostać utworzone: ' . $e->getMessage();
				   }
				}
			}
			catch(PDOException $e) 
			{
			echo 'Wystapil blad biblioteki PDO: ' . $e->getMessage();
			}
		}
		
		
	}
	 

	 
?>
