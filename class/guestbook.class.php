﻿<?php
/**********
author : Łukasz 'caraxes' K. &copy; 2009
**********/
class guestbook {

	  protected $host;
	  protected $user;
	  protected $pwd;
	  protected $dbName;
	 
	 
		 function __construct($host, $user, $pwd, $dbName){
			$this->host = $host;
			$this->user = $user;
			$this->pwd = $pwd;
			$this->dbName = $dbName;
		}
	public function removeVar($var) {
			return str_replace(array(';', '&', '"', "'", '`', '<', '>', 'script', '\ ', '/', '(', ')'), array("", "", "", "", "", "", "", "", "", "", "", ""), $var);
		}
		
	public function guestbook_add() {
			try 
			{
				if(isset($_POST)) 
				{ 	
					try
				   {
					  $pdo = new PDO('mysql:host='.$this->host.';dbname='.$this->dbName.'', ''.$this->user.'', ''.$this->pwd.'', array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
					
					//print_R($_POST);
						$formval = new formValidator();
						
						$formval -> validateEmpty('name','Pole NICK nie może być pozostawione puste',3,55);
						$formval -> validateAlphabetic('name','Pole NICK musi być litera');
						//$formval -> validateEmail('email',EMAIL_BLAD);
						//$formval -> validateEmpty('city_name',MIASTO_PUSTE,3,145);
						//$formval -> validateAlphabetic('city_name',MIASTO_BLAD);
						$formval -> validateEmpty('tresc','Pole treść powinno zawierać od 3 do 500 znaków',3,500);
						
						$_SESSION['dane'] = $_POST;
						
						$formval_errors_number = $formval -> checkErrors();
							if($formval_errors_number > 0) {
								$_SESSION['blad'] = $formval -> displayErrors();
								
									echo "<script>document.location = 'guestbook'</script>";
								
							}	
						$sql = $pdo -> prepare("INSERT INTO `guestbook` (`name`, `content`, `email`, `city`, `www`, `gg`, `date_add`, `login_add`) VALUES
													(
											:cms_guestbook_name,
											:cms_guestbook_content,
											:cms_guestbook_email,
											:cms_guestbook_city,
											:cms_guestbook_www,
											:cms_guestbook_gg,
											NOW(),
											'none'
											);");
						
						$sql -> bindParam(':cms_guestbook_name', $this->removeVar($_POST['name']), PDO::PARAM_STR, 100);
						$sql -> bindParam(':cms_guestbook_content', $this->removeVar($_POST['tresc']), PDO::PARAM_STR, 500);
						$sql -> bindParam(':cms_guestbook_email', $this->removeVar($_POST['email']), PDO::PARAM_STR, 100);
						$sql -> bindParam(':cms_guestbook_city', $this->removeVar($_POST['city_name']), PDO::PARAM_STR, 100);
						$sql -> bindParam(':cms_guestbook_www', $this->removeVar($_POST['www']), PDO::PARAM_STR, 100);
						$sql -> bindValue(':cms_guestbook_gg', $this->removeVar($_POST['gg']), PDO::PARAM_INT);
						
						if($formval_errors_number == 0) {
							$sql -> execute();
						//	print_r($sql ->errorInfo());
							unset($_SESSION['dane']);
						
							//print_r($sql->errorInfo());
							$sql ->closeCursor();
							
							$_SESSION['blad'] = 'Wpis dodano !<br /><br />';
							
								echo "<script>document.location = 'guestbook'</script>";
							
						}
					
				   }
				   catch(PDOException $e)
				   {
					  echo 'Połączenie nie mogło zostać utworzone: ' . $e->getMessage();
				   }
				}
			}
			catch(PDOException $e) 
			{
			echo 'Wystapil blad biblioteki PDO: ' . $e->getMessage();
			}
		}
		
		public function display_entries() {
			try 
			{
			  $pdo = new PDO('mysql:host='.$this->host.';dbname='.$this->dbName.'', ''.$this->user.'', ''.$this->pwd.'', array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
					
					$paging = new paging; 
					
					$sql = $pdo->query("SELECT count(id_guestbook) as count FROM guestbook WHERE status = 1 ");
					$ilosc_wpisy = $sql -> fetch(PDO::FETCH_ASSOC);
					$sql ->closeCursor();
					
					$paging->assign ( 'guestbook' ,  $ilosc_wpisy['count'] , '20' ); 
					
					$sql = $pdo->query("SELECT * FROM `guestbook` WHERE status = 1 ORDER BY date_add DESC LIMIT ".$paging->sql_limit());
						$entries = $sql -> fetchAll(PDO::FETCH_ASSOC);
					
						$sql ->closeCursor();
					if(empty($entries)) echo 'brak danych do wyświetlenia';
					else {
						echo '<p class="plaginacja_top">'.$paging->fetch().'</a></p>';
						foreach($entries as $e) {
						
							if(strpos($e['email'], '@'))
								$mail = '<a href="mailto:'.$e['email'].'">'.$e['name'].'</a>';
							else
								$mail = $e['name'];
							
							if(!empty($e['gg']))
								$gg = ' GG:'.$e['gg'];
							else
								$gg = '';
								
							if(!empty($e['www']))
								$www = 'www: '.$e['www'];
							else
								$www = '';
							
							if(!empty($e['city']))
								$city = 'miasto: '.$e['city'];
							else
								$city = '';
						
							echo '
							<p class="data">Data dodania: '.$e['date_add'].'</p>
							<div class="guest">
							<div class="guest_left">
							<p class="nick">'.$mail.'</p>
							<p class="col">'.$www.'</p>
							<p class="col">'.$gg.'</p>
							<p class="col">'.$city.'</p>
							</div>
							<div class="guest_right">
							<p class="tresc"> '.$e['content'].'</p>
							</div>
							<div class="guest_clear"></div>
							</div>';
						}
						echo '<br /><br /><div class="dane_clearm"></div><p class="plaginacja_bottom">'.$paging->fetch().'</a></p>';
					}
			}
			catch(PDOException $e) 
			{
			echo 'Wystapil blad biblioteki PDO: ' . $e->getMessage();
			}
		}

}
	
?>
