﻿<?php
/**********
author : Łukasz 'caraxes' K. &copy; 2009
**********/
class mail {

	public function send() {
	
		try 
			{
				
									if($_SERVER['REQUEST_METHOD'] == 'POST') 
									{ 

										$formval = new formValidator();
										$formval -> validateEmpty('imie','Podane nazwisko nie mieści się w przedziale od 3 do 55 znaków',3,55);
										$formval -> validateEmpty('comment','Podana treść nie mieści się w przedziale od 10 do 500 znaków',10,500);
										$formval -> validateEmail('email','Podano błędny email ');
										$formval -> validateCaptcha('cap','Proszę podać wynik z obrazka !');

										$formval_errors_number = $formval -> checkErrors();
												if($formval_errors_number > 0)
													echo "<br />".$formval -> displayErrors();
						
											if($formval_errors_number == 0) {
												$email=new sendMail(); 
												$email->html();
												$email->from($_POST['email'], 'Nadawca'); 
												$email->add_recipient('caraxes@gmail.com');//add a recipient in the to: field  
												//$email->add_cc($_POST['email']); // copy to
												$email->subject('Email z formularza kontaktowego ');//set subject 
												$email->message('Imie i nazwisko: '.$_POST['imie'].' tresc: '.$_POST['comment']);//set message body 
												$email->send();//send email(s)
											echo "<br /><p>E-mail został wysłany !</p>";
											unset($_POST);
											}
									}		
								
			}
			catch(PDOException $e) 
			{
			echo 'Błąd, Mail nie został wysłany !';
			}
		
	}

}
	
?>
