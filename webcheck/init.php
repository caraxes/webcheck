<?php

 /*** zainicjuj sesje ***/
 session_start();
 
 /*** timeout na 0 ***/
 set_time_limit(999999);
 
 error_reporting(1);
 ini_set('display_errors', '1');
 
 /*** definiowanie adresu strony ***/
 $site_path = realpath(dirname(__FILE__));
 define ('__SITE_PATH', $site_path); 
 
 /*** wyświetlanie błędów ***/
 include __SITE_PATH . '/application/' . 'my_error_reporting.class.php';
 set_error_handler("myErrorHandler");
 
  /*** ustawienie aktualnej strefy czasowej ***/
 date_default_timezone_set('Europe/Warsaw');
 
  /*** ładowanie kontrollera autoryzacji ***/
 include __SITE_PATH . '/application/' . 'autoryzacja.class.php';
 
 /*** ładowanie klasy controller ***/
 include __SITE_PATH . '/application/' . 'system.class.php';

 /*** ładowanie klasy registry ***/
 include __SITE_PATH . '/application/' . 'registry.class.php';

 /*** ładowanie klasy router ***/
 include __SITE_PATH . '/application/' . 'router.class.php';

 /*** ładowanie klasy template ***/
 include __SITE_PATH . '/application/' . 'template.class.php';

 /*** auto ładowanie modelów ***/
    function __autoload($class_name) {
    $filename = strtolower($class_name) . '.class.php';
    $file = __SITE_PATH . '/model/' . $filename;

    if (file_exists($file))
        include ($file);
	else
		return false;
}
/*** definiowanie danych do bazy mysql ***/
define('HOST', 'localhost');
define('LOGIN', 'user');
define('HASLO', 'test123');
define('BAZA', 'web_check');

 /*** tworzenie nowego obiektu registry ***/
 $registry = new registry;
 
 /*** tworzenie nowego obiektu autoryzacja ***/
 $autoryzacja = new autoryzacja;
 
 /*** sprawdza czy uzytkownik jest zalogowany i odpowiednio działa ***/
 $autoryzacja->sprawdz_usera();
?>
