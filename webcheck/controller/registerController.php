<?php

class registerController extends system {

public function index() {
	
		$this->registry->template->title = 'Witam w formularzu rejestracji';
	$vars = new vars();
	$pdo = new db();

	if(isset($_POST['name'])) {
		include('formValidator.class.php');
		$formval = new formValidator();
			$formval -> validateEmpty('login','Podany nick nie mieści się w zakresie (od 3 do 200 znaków)',3,200);
			$formval -> validateAlphabetic('login','W podanym nicku mogą występować tylko litery i znak _ !');
			$formval -> validateLogin('login','Wybrany nick jest już zajęty');	
			$formval -> validateMail('email','Wybrany email jest już zajęty');	
			$formval -> validateEmail('email','Podany email jest błędny');	
			$formval -> validateEmpty('pass','Podane hasło nie mieści się w zakresie (od 3 do 200 znaków)',3,200);
			$formval -> validateCompare('pass','pass2','Podane hasła nie są takie same !');
				
				$formval_errors_number = $formval -> checkErrors();
					if($formval_errors_number > 0)
						$this->registry->template->error = $formval -> displayErrors();

					if($formval_errors_number == 0) {
						$pdo->register($_POST);
						unset($_POST);
						$this->registry->template->error = 'Konto założone !<br /> Możesz teraz zalogować się <a href="login">TUTAJ</a> i zacząć testować swoją strone<br /><br />';
					}
		
	}
	
	$form = '* - pola wymagane<br />
			<form action="'.$_SERVER['REQUEST_URI'].'" method="post">
			<p>Nick: <input name="login" type="text" value="'.$vars->show('login').'" /> *(używany do logowania)</p>
			<p>Imie: <input name="name" type="text" value="'.$vars->show('name').'" /> (używany do kontaktu)</p>
			<p>E-mail <input name="email" type="text" value="'.$vars->show('email').'" /> *(przy podawaniu maila proszę podać mail z domeny strony która ma zostać przetestowana)</p>
			<p>Hasło: <input name="pass" type="password" value="" />*(używane do logowania)</p>
			<p>Powórz hasło: <input name="pass2" type="password" value="" />*(używane raz do sprawdzenia poprawnosci wprowadzonych haseł)</p>
			<input type="submit" value="Rejestracja" /></p>
			</form>';
		$this->registry->template->form = $form;
	
        $this->registry->template->show('register');
}

}

?>
