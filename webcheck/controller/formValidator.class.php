﻿<?php
/**********
author : Łukasz 'caraxes' K. &copy; 2009
**********/
	 class formValidator {
		private $errors=array();
		public function __construct(){}
		// validate empty field
		public function validateEmpty($field,$errorMessage,$min=4,$max=32){
			if(!isset($_POST[$field])||trim($_POST[$field])==''||strlen($_POST[$field])<$min||strlen($_POST[$field])>$max){
				$this->errors[]=$errorMessage;
			}
		}
		public function validateCheck($field,$errorMessage){
			if(!isset($_POST[$field])||trim($_POST[$field])==''){
				$this->errors[]=$errorMessage;
			}
		}
		public function validateEmptyGET($field,$errorMessage,$min=4,$max=32){
			if(!isset($_GET[$field])||trim($_GET[$field])==''||strlen($_GET[$field])<$min||strlen($_GET[$field])>$max){
				$this->errors[]=$errorMessage;
			}
		}
		// validate integer field
		public function validateInt($field,$errorMessage){
			if(!isset($_POST[$field])||!is_numeric($_POST[$field])||intval($_POST[$field])!=$_POST[$field]){
				$this->errors[]=$errorMessage;
			}
		}
		// validate numeric field
		public function validateNumber($field,$errorMessage){
			if(!isset($_POST[$field])||!is_numeric($_POST[$field])){
				$this->errors[]=$errorMessage;
			}
		}
		// validate if field is within a range
		public function validateRange($field,$errorMessage,$min=1,$max=99){
			if(!isset($_POST[$field])||$_POST[$field]<$min||$_POST[$field]>$max){
				$this->errors[]=$errorMessage;
			}
		}
		// validate alphabetic field
		public function validateAlphabetic($field,$errorMessage){
			if(!isset($_POST[$field])||!preg_match("/^[a-zA-Z_ęóąśłżźćńĘÓĄŚŁŻŹĆŃ]+$/",$_POST[$field])){
				$this->errors[]=$errorMessage;
			}
		}
		
		// validate alphanumeric field
		public function validateAlphanum($field,$errorMessage){
			if(!isset($_POST[$field])||!preg_match("/^[a-zA-Z0-9]+$/",$_POST[$field])){
				$this->errors[]=$errorMessage;
			}
		}
		// validate email
		public function validateEmail($field,$errorMessage){
			if(!isset($_POST[$field])||empty($_POST[$field])||!eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$", $_POST[$field])){
				$this->errors[]=$errorMessage;
			}
		} 
		// validate if email is set in db
		public function validateMail($field,$errorMessage){
		
			$pdo = new db();
			$r = $pdo->checkEmail($_POST[$field]);
			if(!empty($r)){
				$this->errors[]=$errorMessage;
			}
		}
		
		// validate if nick is set in db
		public function validateLogin($field,$errorMessage){
		
			$pdo = new db();
			$r = $pdo->checkLogin($_POST[$field]);
			if(!empty($r)){
				$this->errors[]=$errorMessage;
			}
		}
		// validate passwords compare
		public function validateCompare($field,$field2,$errorMessage){
			if(!isset($_POST[$field])||!isset($_POST[$field2])||empty($_POST[$field])||($_POST[$field] != $_POST[$field2])){
				$this->errors[]=$errorMessage;
			}
		}
			// validate www
		public function validateWWW($field,$errorMessage){
			$www = str_replace('http://', '', $_POST[$field]);
			if(!isset($_POST[$field])||empty($_POST[$field])||!preg_match('|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i', 'http://'.$www)){
				$this->errors[]=$errorMessage;
			}
		} 

		public function validateCaptcha($field,$errorMessage) {
			
			$data = $_SESSION['key'];
			$a = $data['a'];
			$b = $data['b'];
			switch ($data['x']) {
					case '+':
						$odp = $a+$b;
					break;
					case '-':
						$odp = $a-$b;
					break;
					case '*':
						$odp = $a*$b;
					break;
			}
			if(isset($_POST[$field])) {
			  if($_POST[$field] != $odp)
				$this->errors[]=$errorMessage;
			}

		}
		
		
		// check for errors
		public function checkErrors(){
			if(count($this->errors)>0){
				return true;
			}
			return false;
		}
		// return errors
		public function displayErrors(){
		$errorOutput='Znaleziono błędy';
			$errorOutput.='<ul>';
			foreach($this->errors as $err){
				$errorOutput.='<li><p class="blad" style="color:red">'.$err.'</p></li>';
			}
			$errorOutput.='</ul><br />';
			return $errorOutput;
		}
	}
?>