<?php

 include 'init.php';

 /*** ładowanie router ***/
 $registry->router = new router($registry);

 /*** ustawienie adresu dla kontrollerów ***/
 $registry->router->setPath (__SITE_PATH . '/controller');

 /*** ładowanie template ***/
 $registry->template = new template($registry);

 /*** ładowanie controllerów ***/
 $registry->router->loader();



?>
