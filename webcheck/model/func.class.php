<?php

class func {

	private $czas;
	private $bledy;
	
  public function timer($start, $end) {

	$start = strtotime($start);
	$end = strtotime($end);
	
	$dif = $end - $start;
	$minut = sprintf("%01d",floor($dif /60)%60);
	$sekund = sprintf("%01d",floor($dif)%60);
	$godzin = sprintf("%01d",floor($dif/(60*60)%24));
	if($godzin == '0') $h = '';
	else $h = $godzin.' godzin(a/y), ';
	if($minut == '0') $m = '';
	else $m = $minut.' minut(a/y), ';
	if($sekund == '0') $s = '';
	else $s = $sekund.' sekund(a/y)';
	
	return $h.$m.$s;    
	}
	
	public function poziom($lfi,$xss,$urllife,$sqlinjection) {
		
	$_dane['all'] = count($lfi)+count($xss)+count($urllife)+count($sqlinjection);
	
	
	$_dane['bledy_0'] = count($urllife);
	$_dane['bledy_1'] = '0';
	$_dane['bledy_2'] = '0';
	$_dane['bledy_3'] = count($lfi)+count($xss)+count($sqlinjection);
	
		
	if($_dane['bledy_0'] == '0') $_dane['percent_0'] = '0'; else $_dane['percent_0'] = round(($_dane['bledy_0']*100)/$_dane['all']);
	if($_dane['bledy_1'] == '0') $_dane['percent_1'] = '0'; else $_dane['percent_1'] = round(($_dane['bledy_1']*100)/$_dane['all']);
	if($_dane['bledy_2'] == '0') $_dane['percent_2'] = '0'; else $_dane['percent_2'] = round(($_dane['bledy_2']*100)/$_dane['all']);
	if($_dane['bledy_3'] == '0') $_dane['percent_3'] = '0'; else $_dane['percent_3'] = round(($_dane['bledy_3']*100)/$_dane['all']);
	
	$pdo = new db();
	$poziom_bledow = $pdo->getErrorStateInfo();
	
	if($_dane['percent_3'] >= 20 ) { 
		$_dane['poziom'] = "3";
		$_dane['poziom_info'] = $poziom_bledow['web_errors_dict_info_state_one'];
	}
	elseif($_dane['percent_2'] >= 30 ) { 
		$_dane['poziom'] = "2";
		$_dane['poziom_info'] = $poziom_bledow['web_errors_dict_info_state_two'];
	}
	elseif($_dane['percent_1'] >= 30 ) { 
		$_dane['poziom'] = "1"; 
		$_dane['poziom_info'] = $poziom_bledow['web_errors_dict_info_state_three'];
	}
	elseif($_dane['percent_0'] >= 20 ) { 
		$_dane['poziom'] = "0";
		$_dane['poziom_info'] = $poziom_bledow['web_errors_dict_info_state_four'];
	}
	elseif($_dane['percent_0'] == 0 AND $_dane['percent_1'] == 0 AND $_dane['percent_2'] == 0 AND $_dane['percent_3'] == 0 ) { 
		$_dane['poziom'] = "0";
		$_dane['poziom_info'] = $poziom_bledow['web_errors_dict_info_state_four'];
	}
	
	
	
	$_dane['znaczek'] = "&nbsp;";
	
	return $_dane;
		
	}
	

}

?>
