<?php

class db {

	public $pdo;
    
    public function __construct() {
		try {
        $this->pdo = new PDO('mysql:host='.HOST.';dbname='.BAZA.'', ''.LOGIN.'', ''.HASLO.'', array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",  PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true)); //PDO::MYSQL_ATTR_MAX_BUFFER_SIZE=>1024*1024*50,
        $this->pdo -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
		catch (Exception $e) {
		echo "Failed: " . $e->getMessage();
		}
    }
	
	
	
	/**
	*
	* pobiera klucz dla wybranego uzytkownik
	* 
	* @return boloean
	**/
	public function getUrlCode($id) {
 
        try {

          $sql = $this->pdo -> query("SELECT web_code_name FROM web_code WHERE id_web_user = '".$id."' AND web_code_state = 1");
						
			$result = $sql->fetch(PDO::FETCH_ASSOC);
			$sql ->closeCursor();
			return $result['web_code_name'];
            
        } catch (PDOException $e) {
            die( 'Error1:' . $e->getMessage());
        }
        
    }
	
	/**
	*
	* zapisuje klucz do bazy pod is danego uzytkownika
	* 
	* @return boloean
	**/
	public function setUrlCode($id,$url_code) {
 
        try {
			
			$sql = $this->pdo -> exec("UPDATE web_code SET web_code_state = 0 WHERE id_web_user = '".$id."'");
				
			$sql = $this->pdo -> exec("INSERT INTO `web_code` (`id_web_user`, `web_code_name`, `web_code_state`) 
								VALUES ('".$id."', '".$url_code."', '1')");
				return true;			
            
        } catch (PDOException $e) {
            die( 'Error2:' . $e->getMessage());
        }
        
    }

	/**
	*
	* Dodaje wpis do bazy dotyczący określonego działania na stronie
	* 
	* @param string $user nazwa usera pobrana z formulzarza
	* @param string $pass pass usera pobrana z formulzarza
	* @return array or error
	**/
	public function login($user,$pass) {
 
        try {
			
			$user = trim($user);
			$pass = trim(md5($pass));

           $sql = $this->pdo -> prepare("SELECT * FROM web_user WHERE web_user_login = :mail  AND web_user_pass = :pass");
						$sql -> bindValue(':mail', $user, PDO::PARAM_STR);
						$sql -> bindValue(':pass', $pass, PDO::PARAM_STR);
						$sql -> execute();
						
			$result = $sql->fetch(PDO::FETCH_ASSOC);
			$sql ->closeCursor();
			
			if(!empty($result) || $result != 0) {
				$this->pdo -> exec("UPDATE web_user SET last_login = NOW() WHERE web_user_login = '".$user."'");
			}
			return $result;
            
        } catch (PDOException $e) {
            die( 'Error3:' . $e->getMessage());
        }
        
    }
	
	/**
	*
	* sprawdza czy podany mail jest wolny
	* 
	* @return boloean
	**/
	public function checkEmail($email) {
 
        try {

          $sql = $this->pdo -> query("SELECT * FROM web_user WHERE web_user_email = '".$email."'");
						
			$result = $sql->fetchAll(PDO::FETCH_ASSOC);
			$sql ->closeCursor();
			return $result;
            
        } catch (PDOException $e) {
            die( 'Error4:' . $e->getMessage());
        }
        
    }
	
	/**
	*
	* sprawdza czy podany login jest wolny
	* 
	* @return boloean
	**/
	public function checkLogin($login) {
 
        try {

          $sql = $this->pdo -> query("SELECT * FROM web_user WHERE web_user_login = '".$login."'");
						
			$result = $sql->fetchAll(PDO::FETCH_ASSOC);
			$sql ->closeCursor();
			return $result;
            
        } catch (PDOException $e) {
            die( 'Error5:' . $e->getMessage());
        }
        
    }
	
	/**
	*
	* Pobiera dane słownikowe do ataku XSS
	* 
	* @return array or error
	**/
	public function getXssData() {
 
        try {

          $sql = $this->pdo -> query("SELECT * FROM xss_input_code");
						
			$result = $sql->fetchAll(PDO::FETCH_ASSOC);
			$sql ->closeCursor();
			return $result;
            
        } catch (PDOException $e) {
            die( 'Error6:' . $e->getMessage());
        }
        
    }
	
	
	/**
	*
	* Zapisuje dane po ataku XSS
	* @param string $id_url id url
	* @param string $url zainfekowany url  
	* @param int $type typ ataku
	*
	* @return array or error
	**/
	public function saveXssData($id_url, $url, $type) {
		try {
		$sql = $this->pdo -> exec("INSERT INTO `xss_result` (`id_web_url` ,`xss_result_name`, `xss_result_type` )
									VALUES ('".$id_url."', '".$url."', '".$type."');");
			return true;
		    
        } catch (PDOException $e) {
            die( 'Error7:' . $e->getMessage());
        }
	}

	/**
	*
	* Pobiera dane po ataku XSS
	* @param string $id_url id url do pobrania
	* @param int $type typ ataku
	*
	* @return array or error
	**/
	public function returnXssData($id_url) {
		try {
			$id_url = intval($id_url);
			$sql = $this->pdo -> prepare("SELECT * FROM `xss_result` WHERE id_web_url= :id");
						$sql -> bindValue(':id', $id_url, PDO::PARAM_INT);
						$sql -> execute();
			$result = $sql->fetchAll(PDO::FETCH_ASSOC);
			$sql ->closeCursor();
			return $result;		    
        } catch (PDOException $e) {
            die( 'Error8:' . $e->getMessage());
        }
	}
	
	/**
	*
	* Zapisuje dane po ataku SQL Injection
	* @param string $url zainfekowany url  
	*
	* @return array or error
	**/
	public function saveSqlInjectionData($id_url, $url_xss, $data) {
		try {
		$sql = $this->pdo -> exec("INSERT INTO `sql_injection_url_result` (`id_web_url`, `sql_injection_url_result_infected_url`, `sql_injection_url_result_data`)
									VALUES ('".$id_url."', '".$url_xss."', '".$data."');");
			return true;
		    
        } catch (PDOException $e) {
            die( 'Error9:' . $e->getMessage());
        }
	}
	
	/**
	*
	* Pobiera dane po ataku SQL Injection
	* @param string $id_url id url do pobrania
	*
	* @return array or error
	**/
	public function getSqlInjectionData($id_url) {
		try {
			$id_url = intval($id_url);
			$sql = $this->pdo -> prepare("SELECT * FROM `sql_injection_url_result` WHERE id_web_url= :id");
						$sql -> bindValue(':id', $id_url, PDO::PARAM_INT);
						$sql -> execute();
			$result = $sql->fetchAll(PDO::FETCH_ASSOC);
			$sql ->closeCursor();
			return $result;		    
        } catch (PDOException $e) {
            die( 'Error10:' . $e->getMessage());
        }
	}
	
	
	/**
	*
	* Zapisuje dane nowego usera do bazy
	* 
	* @return array or error
	**/
	public function register($data) {
 
        try {
			$sql = $this->pdo -> prepare("INSERT INTO `web_user` (`web_user_name`, `web_user_login`, `web_user_pass`, `web_user_email`, `reg_date`) VALUES
								(
								:name,
								:login,
								:pass,
								:email,
								NOW());");
					$pass = md5($data['pass']);
					  $sql -> bindParam(':name', $data['name'], PDO::PARAM_STR, 200);
					  $sql -> bindParam(':login', $data['login'], PDO::PARAM_STR, 200);
					  $sql -> bindParam(':pass', $pass, PDO::PARAM_STR, 200);
					  $sql -> bindParam(':email', $data['email'], PDO::PARAM_STR, 300);

			$sql -> execute();
			$sql -> closeCursor();
			return true;
            
        } catch (PDOException $e) {
            die( 'Error11:' . $e->getMessage());
        }
        
    }
	
	/**
	*
	* Zapisuje dane po ataku input Xss
	* @param string $id_url id zainfekowanego url
	* @param string $vars zmienne zainfekowane wyslane postem
	* 
	* @return array or error
	**/
	public function saveXssInputResult($id_url, $vars) {
 
        try {
			///TODO: id_web_url .... hmmmmm
          $sql = $this->pdo -> exec("INSERT INTO `xss_input_result` ( `id_web_url`, `xss_input_result_infected_var`) VALUES
									(".$id_url.", '".$vars."')");
									
			return true;
            
        } catch (PDOException $e) {
            die( 'Error12:' . $e->getMessage());
        }
        
    }
	
	/**
	*
	* Odczytauje dane po ataku input Xss
	* @param string $id_url id zainfekowanego url
	* @param string $vars zmienne zainfekowane wyslane postem
	* 
	* @return array or error
	**/
	public function getXssInputResult($id_url) {
 
        try {
          $id_url = intval($id_url);
			$sql = $this->pdo -> prepare("SELECT * FROM `xss_input_result` WHERE id_web_url= :id");
						$sql -> bindValue(':id', $id_url, PDO::PARAM_INT);
						$sql -> execute();
			$result = $sql->fetchAll(PDO::FETCH_ASSOC);
			$sql ->closeCursor();
			return $result;
            
        } catch (PDOException $e) {
            die( 'Error13:' . $e->getMessage());
        }
        
    }
	
	/**
	*
	* Dodaje wpis do bazy dotyczący określonego działania na stronie
	* 
	* @param int $user_id Id usera który jest aktualnie zalogowany
	* @param string $who Kto jest zalogowany ? worker - pracownik CRM'u czy user - pracownik biura
	* @param int $action FK wykonanej akacji z tabeli crm_logs_action
	* @return boolean or error
	**/
	public function addLog($user_id,$who,$action) {
 
        try {
 
            $sql = $this->pdo -> exec("INSERT INTO crm_logs_".$who." (id_crm_".$who.", id_crm_logs_action, crm_logs_worker_date, crm_logs_".$who."_ip, crm_logs_".$who."_host) VALUES 
					(".$user_id.", ".$action.", NOW(), '".$_SERVER['REMOTE_ADDR']."', '".gethostbyaddr($_SERVER['REMOTE_ADDR'])."')");
			//$result = $sql->fetchAll(PDO::FETCH_ASSOC);
            return true;
            
        } catch (PDOException $e) {
            die( 'Error14:' . $e->getMessage());
        }
        
    }
	
	/**
	*
	* Pobiera dane zalogowanej osoby
	* 
	* @return array or error
	**/
	public function getUserInfo() {
 
        try {
           $sql = $this->pdo -> prepare("SELECT * FROM crm_worker WHERE id_crm_worker= :id AND crm_worker_status = 1");
						$sql -> bindValue(':id', $_SESSION['user_id'], PDO::PARAM_STR);
						$sql -> execute();
			$result = $sql->fetch(PDO::FETCH_ASSOC);
			print_R($result); die('a teraz tu');
			$sql ->closeCursor();
			return $result;
            
        } catch (PDOException $e) {
            die( 'Error15:' . $e->getMessage());
        }
        
    }
	
	/**
	*
	* Dodaje wpis do bazy dotyczący szczegolow urla na poczatku
	* 
	* @param string $url nazwa url do skoanowania
	* @param array $http_head dane z head 
	*
	* @return boolean or error
	**/
	public function addStartInfo($url,$http_head) {
 
        try {
 
			$user_id = $_SESSION['user_id'];
			//echo '<p>START - dodaje do bazy info o url</p>';

			$urll = str_replace('http://', '', str_replace('www.', '', $url));
			$urll = explode('/', $urll);
			$urlll = explode('?', $urll[0]);
			$url_ip = gethostbyname($urlll[0]);
			$protocol = $http_head[0];
			foreach ($http_head as $k=>$v) {
			$http_head[$k] = explode(":", $v, 2);
				if (strtolower($http_head[$k][0]) == "content-type") {
					$kodowanie = $http_head[$k][1];
				}
				if (strtolower($http_head[$k][0]) == "server") {
					$serwer_info = $http_head[$k][1];
				}
			}
			//if(!isset($serwer_info)) $serwer_info = 'brak danych';
			//if(!isset($kodowanie)) = $kodowanie = 'brak danych';
			
			
			$sql = $this->pdo -> exec("INSERT INTO `web_url` (
						`id_web_user` ,
						`web_url_name` ,
						`web_url_ip` ,
						`web_url_date_start` ,
						`web_url_server_info` ,
						`web_url_server_protocol` ,
						`web_url_http_charset` 
						)
						VALUES (".$user_id.", '".$url."', '".$url_ip."', NOW(), '".$serwer_info."', '".$protocol."', '".$kodowanie."')");
				
            return $this->pdo -> lastInsertId();
            
        } catch (PDOException $e) {
            die( 'Error16:' . $e->getMessage());
        }
        
    }
	
	/**
	*
	* Pobiera dane z bazy na temat URL
	* 
	* @param string $id_url id url skanowania z bazy
	*
	* @return boolean or error
	**/
	public function getUrlInfo($id_url) {
 
        try {
		$id_url = intval($id_url);
			$sql = $this->pdo -> prepare("SELECT * FROM web_url WHERE id_web_url= :id");
						$sql -> bindValue(':id', $id_url, PDO::PARAM_INT);
						$sql -> execute();
			$result = $sql->fetch(PDO::FETCH_ASSOC);
			$sql ->closeCursor();
			return $result;
            
        } catch (PDOException $e) {
            die( 'Error17:' . $e->getMessage());
        }
        
    }
	
	/**
	*
	* Zapsiuje dane ataku LFI
	* 
	* @return array or error
	**/
	public function saveLfiResult($id_url, $url_lfi) {
 
        try {
          $sql = $this->pdo -> exec("INSERT INTO `lfi_result` (`id_web_url` ,`lfi_result_url_attack`)
									VALUES ('".$id_url."', '".$url_lfi."');");
			return true;
            
        } catch (PDOException $e) {
            die( 'Error18:' . $e->getMessage());
        }
        
    }
	
	/**
	*
	* Odczytauje dane ataku LFI
	* 
	* @return array or error
	**/
	public function getLfiResult($id_url) {
 
        try {
          $id_url = intval($id_url);
			$sql = $this->pdo -> prepare("SELECT * FROM `lfi_result` WHERE id_web_url= :id");
						$sql -> bindValue(':id', $id_url, PDO::PARAM_INT);
						$sql -> execute();
			$result = $sql->fetchAll(PDO::FETCH_ASSOC);
			$sql ->closeCursor();
			return $result;
            
        } catch (PDOException $e) {
            die( 'Error19:' . $e->getMessage());
        }
        
    }
	
	/**
	*
	* Zapisuje dane po sprawdzeniu URLi
	* @param string $id_url id urla z bazy
	* @param string $url_code kod bledu
	* @param string $url zainfekowany url  
	*
	* @return array or error
	**/
	public function saveUrlLifeData($id_url, $url_code, $url) {
		try {
		$sql = $this->pdo -> exec("INSERT INTO `url_life_result` (`id_web_url` ,`url_life_result_url` ,`url_life_result_error_code` )
						VALUES ( ".$id_url.", '".$url."', '".$url_code."')");
		unset($sql);
			return true;
		    
        } catch (PDOException $e) {
            die( 'Error20:' . $e->getMessage());
        }
	}
	
	/**
	*
	* Pobiera dane po sprawdzeniu URLi
	* @param string $id_url id url do pobrania
	*
	* @return array or error
	**/
	public function getUrlLifeData($id_url) {
		try {
			$id_url = intval($id_url);
			$sql = $this->pdo -> prepare("SELECT * FROM `url_life_result` WHERE id_web_url= :id");
						$sql -> bindValue(':id', $id_url, PDO::PARAM_INT);
						$sql -> execute();
			$result = $sql->fetchAll(PDO::FETCH_ASSOC);
			$sql ->closeCursor();
			return $result;		    
        } catch (PDOException $e) {
            die( 'Error21:' . $e->getMessage());
        }
	}
	
	/**
	*
	* Pobiera dane słownikowe blędów
	*
	* @return array or error
	**/
	public function getErrorsInfo() {
 
        try {
			$sql = $this->pdo -> prepare("SELECT * FROM `web_errors_dict_details` INNER JOIN web_errors_dict_details_type ON web_errors_dict_details.id_web_errors_dict_details_type = web_errors_dict_details_type.id_web_errors_dict_details_type");
			$sql -> execute();
			$result = $sql->fetchAll(PDO::FETCH_ASSOC);
			$sql ->closeCursor();
			return $result;		    
            
        } catch (PDOException $e) {
            die( 'Error22:' . $e->getMessage());
        }
        
    }
	
	/**
	*
	* Pobiera opis stanu raportu
	*
	* @return array or error
	**/
	public function getErrorStateInfo() {
 
        try {
			$sql = $this->pdo -> prepare("SELECT * FROM `web_errors_dict_info_state`");
			$sql -> execute();
			$result = $sql->fetch(PDO::FETCH_ASSOC);
			$sql ->closeCursor();
			return $result;		    
            
        } catch (PDOException $e) {
            die( 'Error23:' . $e->getMessage());
        }
        
    }
	
	/**
	*
	* Dodaje wpis do bazy dotyczący szczegolow urla przy zakonczeniu
	* 
	* @param string $id_url id url do skanowania z bazy
	*
	* @return boolean or error
	**/
	public function addEndInfo($id_url) {
 
        try {
 
			$sql = $this->pdo->prepare("UPDATE `web_url` SET `web_url_date_end` = NOW() WHERE `id_web_url` = '".$id_url."'");
				$sql -> execute();
			$sql ->closeCursor();
			//print_R($sql->errorInfo()); //die();
				//return true;
            
        } catch (PDOException $e) {
            die( 'Error24:' . $e->getMessage());
        }
        
    }
	
}

?>
