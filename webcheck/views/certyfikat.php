<?php
session_start();

// if(!isset($_COOKIE['_url'])) die('Nie masz prawa TY BYC!');

// Set the content-type
header('Content-type: image/jpeg; charset=utf-8');

// Create the image
$im = @imagecreatefromjpeg('../img/certyfikat.jpg');

// Create some colors
$white = imagecolorallocate($im, 255, 255, 255);
$grey = imagecolorallocate($im, 128, 128, 128);
$black = imagecolorallocate($im, 0, 0, 0);
$red = imagecolorallocate($im, 255, 0, 0);

// The text to draw
$date = date('Y-m-d H:i');
$url = $_COOKIE['_url'];
// unset($_SESSION['_url']);
// Replace path by your own font path
$font = 'verdana.ttf';

// Add the text
if(strlen($url) > 53) {
	imagettftext($im, 9, 0, 130, 160, $black, $font, substr($url, 0, 53));
	imagettftext($im, 9, 0, 92, 180, $black, $font, substr($url, 53, 55));
	if(strlen($url) > 116)
		imagettftext($im, 9, 0, 515, 180, $black, $font, '...');
} else 
imagettftext($im, 9, 0, 130, 160, $black, $font, $url);

imagettftext($im, 9, 0, 288, 212, $black, $font, $date);

$a = 'Cross-site scripting';
$b = 'SQL Injection';
$c = 'Local file Include';
$d = 'Prawidlowe adresy URL';

imagettftext($im, 10, 0, 280, 250, $black, $font, $a);
imagettftext($im, 10, 0, 280, 270, $black, $font, $b);
imagettftext($im, 10, 0, 280, 290, $black, $font, $c);
imagettftext($im, 10, 0, 280, 310, $black, $font, $d);

imagettftext($im, 16, 20, 400, 345, $red, $font, 'ZALICZONE');

$no = @imagecreatefromjpeg('../img/no.jpg');
$ok = @imagecreatefromjpeg('../img/ok.jpg');

$mini = imagecreatetruecolor(100, 100);

if (in_array('xss', $_SESSION['metod'])) {
	imagecopyresized($mini, $ok, 0, 0, 0, 0, 20, 20, imagesx($ok), imagesy($ok));
	imagecopymerge($im, $mini, 255, 235, 0, 0, 20, 20, 100);
} else {
	imagecopyresized($mini, $no, 0, 0, 0, 0, 20, 20, imagesx($no), imagesy($no));
	imagecopymerge($im, $mini, 255, 235, 0, 0, 20, 20, 100);
}
if (in_array('si', $_SESSION['metod'])) {
	imagecopyresized($mini, $ok, 0, 0, 0, 0, 20, 20, imagesx($ok), imagesy($ok));
	imagecopymerge($im, $mini, 255, 255, 0, 0, 20, 20, 100);
} else {
	imagecopyresized($mini, $no, 0, 0, 0, 0, 20, 20, imagesx($no), imagesy($no));
	imagecopymerge($im, $mini, 255, 255, 0, 0, 20, 20, 100);
}
if (in_array('lu', $_SESSION['metod'])) {
	imagecopyresized($mini, $ok, 0, 0, 0, 0, 20, 20, imagesx($ok), imagesy($ok));
	imagecopymerge($im, $mini, 255, 275, 0, 0, 20, 20, 100);
} else {
	imagecopyresized($mini, $no, 0, 0, 0, 0, 20, 20, imagesx($no), imagesy($no));
	imagecopymerge($im, $mini, 255, 275, 0, 0, 20, 20, 100);
}
if (in_array('ul', $_SESSION['metod'])) {
	imagecopyresized($mini, $ok, 0, 0, 0, 0, 20, 20, imagesx($ok), imagesy($ok));
	imagecopymerge($im, $mini, 255, 295, 0, 0, 20, 20, 100);
} else {
	imagecopyresized($mini, $no, 0, 0, 0, 0, 20, 20, imagesx($no), imagesy($no));
	imagecopymerge($im, $mini, 255, 295, 0, 0, 20, 20, 100);
}

// Using imagepng() results in clearer text compared with imagejpeg()
imagejpeg($im);
imagedestroy($im);
?>