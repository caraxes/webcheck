<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>USER</title>
<meta name="description" content="" />
<meta name="keywords" content="" />
<meta name="author" content="caraxes" />
<meta name="copyright" content="caraxes" />
<meta http-equiv="Content-Language" content="pl" />
<meta name="robots" content="index,follow" />
<meta name="revisit-after" content="2 days" />
<style type="text/css">
p.error {
color: #c00;
font-weight: bold;
}
p.http1 {
color: #00c;
font-weight: bold;
}
p.http2 {
color: #0c0;
font-weight: bold;
}
p.http3 {
color: #cc0;
font-weight: bold;
}
p.http4 {
color: #c00;
font-weight: bold;
}
p.http5 {
color: #c60;
font-weight: bold;
}
pre, code {
width: 90%;
border: 1px solid #ddd;
background: #eee;
margin: 0 auto;
white-space:pre-wrap;
}
pre.output {
height: 400px;
overflow: auto;
}
</style>
</head>
<body>
<div id="strona">
<h1><?php echo $title ?></h1>

<?php
if(isset($error)) echo $error;
 echo $form; ?>

</div>

</body>
</html>