<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>CRX Web Check</title>
<meta name="description" content="" />
<meta name="keywords" content="" />
<meta name="author" content="caraxes" />
<meta name="copyright" content="caraxes" />
<meta http-equiv="Content-Language" content="pl" />
<meta name="robots" content="index,follow" />
<meta name="revisit-after" content="2 days" />
<link rel="stylesheet" href="css.css" type="text/css" />
</head>
<body>
<div id="strona">
<?php
	$func = new func;
	//print_r($diagram);
?>
<h2>Skanowanie <?php echo $urldata['web_url_name']." (".$urldata['web_url_ip'].")"; ?></h2><hr><br />
<h3>Wyniki skanowania</h3><hr><br />
<table class="info">
<tr><td colspan="2" class="info_top">Informacje o skanowaniu</td></tr>
<tr><td class="info_left">Data startu skanowania</td><td class="info_right"><?php echo $urldata['web_url_date_start']; ?></td></tr>
<tr><td class="info_left">Data końca skanowania</td><td class="info_right"><?php echo $urldata['web_url_date_end']; ?></td></tr>
<tr><td class="info_left">Łączny czas skanowania</td><td class="info_right"><?php echo $func->timer($urldata['web_url_date_start'], $urldata['web_url_date_end']); ?></td></tr>
<tr><td class="info_left">Wybrane testy</td><td class="info_right"><?php 
if(in_array('xss', $_SESSION['metod'])) echo ' XSS, '; 
if(in_array('si', $_SESSION['metod'])) echo ' SQL Injection, '; 
if(in_array('lu', $_SESSION['metod'])) echo ' LFI, '; 
if(in_array('ul', $_SESSION['metod'])) echo ' URL LIFE, ';
?></td></tr>
</table>
<table class="info">
<tr><td colspan="2" class="info_top">Informacje o serwerze</td></tr>
<tr><td class="info_left">Czułość skanowania</td><td class="info_right"><?php if(count($_SESSION['metod']) == '4') echo 'dokładna'; else echo 'pobieżna'; ?></td></tr>
<tr><td class="info_left">Dane 'server info'</td><td class="info_right"><?php echo$urldata['web_url_server_info']; ?></td></tr>
<tr><td class="info_left">Protokół serwera</td><td class="info_right"><?php echo $urldata['web_url_server_protocol']; ?></td></tr>
<tr><td class="info_left">Kodowania znaków</td><td class="info_right"><?php echo $urldata['web_url_http_charset']; ?></td></tr>
</table>
<br />
<h3>Poziom zagrożenia</h3><hr><br />
<table class="poziom">
<tr><td class="poziom_img"><img style="width:200px" src="img/poziomy/<?php echo $diagram['poziom']; ?>.jpg" name="poziom bezpieczeństwa"></td><td class="poziom_info"><?php echo $diagram['poziom_info']; ?></td></tr>
</table>
<br /><br />
<h3>Rozkład zagrożeń / alarmów bezpieczeństwa</h3><hr><br />
<table class="alerts">
<tr><td colspan="2" class="alerts_info">Suma znalezionych zagrożeń</td><td class="alerts_ile" style="font-weight:bold;"><?php echo $diagram['all'] ?></td><td>&nbsp;</td></tr>
<tr><td class="alerts_img"><img src="img/alerts/3.jpg"></td><td class="alert_hight">Wysoki</td><td class="alerts_ile"><?php echo $diagram['bledy_3'] ?></td><td><span class="alerts_graf1"><?php if($diagram['percent_3']!='0') echo '<span style="color:#fff">'.$diagram['percent_3'].'%</span>'; for($i=0;$i<$diagram['percent_3'];$i++) echo $diagram['znaczek']; ?></span></td></tr>
<tr><td class="alerts_img"><img src="img/alerts/2.jpg"></td><td class="alert_medium">Średni</td><td class="alerts_ile"><?php echo $diagram['bledy_2'] ?></td><td><span class="alerts_graf2"><?php if($diagram['percent_2']!='0') echo '<span style="color:#fff">'.$diagram['percent_2'].'%</span>'; for($i=0;$i<$diagram['percent_2'];$i++) echo $diagram['znaczek']; ?></span></td></tr>
<tr><td class="alerts_img"><img src="img/alerts/1.jpg"></td><td class="alert_low">Niski</td><td class="alerts_ile"><?php echo $diagram['bledy_1'] ?></td><td><span class="alerts_graf3"><?php if($diagram['percent_1']!='0') echo '<span style="color:#fff">'.$diagram['percent_1'].'%</span>'; for($i=0;$i<$diagram['percent_1'];$i++) echo $diagram['znaczek']; ?></span></td></tr>
<tr><td class="alerts_img"><img src="img/alerts/0.jpg"></td><td class="alert_inf">Informacja</td><td class="alerts_ile"><?php echo $diagram['bledy_0'] ?></td><td><span class="alerts_grafi"><?php if($diagram['percent_0']!='0') echo '<span style="color:#fff">'.$diagram['percent_0'].'%</span>'; for($i=0;$i<$diagram['percent_0'];$i++) echo $diagram['znaczek']; ?></span></td></tr>
</table>
<br /><br />
<!--
<h3>Zestawienie błędów</h3><hr><br />
<table class="list">
<tr><td class="alert_img"><img src="img/alerts/1.jpg"></td><td class="list_name"> Apache Mod_Rewrite Off-By-One Buffer Overflow Vulnerability</td></tr>
<tr><td colspan=2 class="list_menu"> Miejsce</td><td class="list_menu">Ilość wystąpień</td></tr>
<tr><td colspan=2 class="lista"> /guestbook.php</td><td class="lista_ile">2</td></tr>
</table> 

<br /><br />
-->
<?php

echo '<h3>Detale błędów</h3><hr><br />';

if(!empty($lfidata)) echo $lfi;
if(!empty($sqlinjection)) echo $sql_injection;
if(!empty($xssdata)) echo $xss;
if(!empty($urllifedata)) echo $url_life;

?>
</div>
</body>
</html>

