<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>CRX WebCheck - Darmowy skaner bezpieczeństwa aplikacji internetowych</title>
<meta name="description" content="Darmowy skaner bezpieczeństwa aplikacji internetowych, sprawdź czy twoja strona jest zabezpieczona przed atakami XSS, SQL Injection, LFI ..." />
<meta name="keywords" content="bezpieczeństwo, web scanner, free, darmowy, skaner, webcheck, xss, lfi, rfi, injection, sql, aplikacja, www" />
<meta name="author" content="caraxes" />
<meta name="copyright" content="2009/2010 by caraxes" />
<meta http-equiv="reply-to" content="caraxes@gmail.com">
<meta http-equiv="Content-Language" content="pl, en" />
<meta name="robots" content="index,follow" />
<meta name="revisit-after" content="14 days" />
<style type="text/css">
p.error {
color: #c00;
font-weight: bold;
}
p.http1 {
color: #00c;
font-weight: bold;
}
p.http2 {
color: #0c0;
font-weight: bold;
}
p.http3 {
color: #cc0;
font-weight: bold;
}
p.http4 {
color: #c00;
font-weight: bold;
}
p.http5 {
color: #c60;
font-weight: bold;
}
pre, code {
width: 90%;
border: 1px solid #ddd;
background: #eee;
margin: 0 auto;
white-space:pre-wrap;
}
pre.output {
height: 400px;
overflow: auto;
}
</style>
</head>
<body>
<div id="strona">

Witaj, jesteś zalogowany - <a href="login/loggout">wyloguj </a><br /> 

<br />
<?php
if(isset($code)) echo 'Oto twój unikalny klucz tej sesji :'.$code.' Proszę wkleić podany niżej fragment kodu do kodu swojej strony, pozwoli to sprawdzić czy jesteś jej autorem.<br />
<textarea rows="1" cols="45" style="overflow:hidden;"><!--'.str_replace('<br />','',$code).'--></textarea><br />
<b>Proszę pamiętać, że długość życia klucza to sesja przeglądarki.</b>';
?>
<div><br />
Instrukcja:<ol>
<li>Wprowadź w kod swojej strony wyżej wygenerowany kod.</li>
<li>Wprowadź adres URL w pole "Adres do przetestowania"</li>
<li>Zaakceptuj postanowienia regulaminu</li>
<li>Wybierz metody ataków (aby skrypt był skuteczny proszę wprowadzać adresy w których są zmienne, np: http://strona.pl?id=12&amp;podstrona=index)</li>
<li>Kliknij w przycisk "Sprawdź" a następnie cierpliwie czekaj na wynik skanowania </li>
</ol>
</div>


	<form method="post" action="<?php echo $_SERVER['REQUEST_URI']?>"><div>
    <p>Adres do przetestowania:</p><input type="text" id="host" size="50" name="host" value="<?php if(isset($host)) echo $host; ?>"><br>
	<p><input type="checkbox" name="regulamin" value="1"> Akceptuje postanowienia <a href="../wc/rules" target="_blank">regulaminu</a></p>
    <p>Metody ataku</p><ul>
	<li><input type="checkbox" name="metod[]" value="xss"> <b>Cross-site scripting (XSS) </b>– sposób ataku na serwis WWW polegający na osadzeniu w treści atakowanej strony kodu, który wyświetlony innym użytkownikom może doprowadzić do wykonania przez nich niepożądanych akcji.</li>
	<li><input type="checkbox" name="metod[]" value="si"> <b>SQL Injection (z ang., dosłownie zastrzyk SQL)</b> – luka w zabezpieczeniach aplikacji internetowych polegająca na nieodpowiednim filtrowaniu lub niedostatecznym typowaniu i późniejszym wykonaniu danych przesyłanych w postaci zapytań SQL do bazy danych.</li>
	<li><input type="checkbox" name="metod[]" value="lu"> <b>Local file Include (LFI) </b>- sprawdzenie czy na stronie możliwy jest dostęp do zabezpieczonych danych np plik etc/passwd</li>
	<li><input type="checkbox" name="metod[]" value="ul"> <b>URL Life </b>- sprawdzi czy na podanej stronie są błędne linki</li>
	<ul><br />
    <input type="submit" value="Sprawdź">
    </div></form>
	<?php 
	if(isset($error)) echo $error;
	//print_R($_POST);
	 ?>
    <hr>
	<?php if(isset($url_data)) echo '<pre class="output">'.$url_data.'</pre>'; ?>
	
</div>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-13116613-5");
pageTracker._trackPageview();
} catch(err) {}</script>
</body>
</html>

