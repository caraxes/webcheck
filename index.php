<?php
//session start
session_start();

//header ('Content-type: text/html; charset=utf-8');
//print_R($_SESSION);
// set session life time
ini_set('session.gc_maxlifetime', '300'); // 300 sec = 5 min


//autoload user-defined classes
      function __autoload($class){
          include 'class/'.$class.'.class.php';
      }

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>CRX WebCheck &raquo; <?php echo $_GET['goto'] ?> - Darmowy skaner bezpieczeństwa aplikacji internetowych</title>
<meta name="description" content="Darmowy skaner bezpieczeństwa aplikacji internetowych, sprawdź czy twoja strona jest zabezpieczona przed atakami XSS, SQL Injection, LFI ..." />
<meta name="keywords" content="webcheck, crx, bezpieczeństwo, web scanner, free, darmowy, skaner, xss, lfi, rfi, injection, sql, aplikacja www" />
<meta name="author" content="caraxes" />
<meta name="copyright" content="2009/2010 by caraxes" />
<meta http-equiv="reply-to" content="caraxes@gmail.com" />
<meta http-equiv="Content-Language" content="pl, en" />
<meta name="robots" content="index,follow" />
<meta name="revisit-after" content="14 days" />
<script type="text/javascript" src="jquery.js"></script>
<style type="text/css">
@import url(css.css);
</style>
<script type="text/javascript" src="jsa.js"></script>
</head>
<body>
    <div id="wrapper">
    <h1>CRX Web Check</h1>
    <ul id="nav">
		<li><a href="news">Nowości</a></li>
        <li><a href="about">O autorze i skrypcie</a></li>
        <li><a href="rules">regulamin</a></li>
		<li><a href="contact">kontakt</a></li>
		<li><a href="check">Sprawdź bezpieczeństwo swojej strony</a></li>
    </ul>
    <div id="content">    	
		<?php
		$vars = new vars();
		switch ($_GET['goto']) {
		case 'news':
			echo '<h2>Nowości</h2>'; 
			echo '<h3>Propozycja współpracy</h3><span style="text-align:right; color:#fff; font-size:11px">16 październik 2013</span>
			<p>Z racji, że przez te 3 lata od kiedy powstał skrypt ponad połowa stron przebadanych dalej jest niezabezpieczona postanowiłem przepisać aplikację. Dlatego poszukuje osób zainteresowanych współpracą w przebudowaniu i rozszerzeniu możliwości skryptu. Jeśli masz jako takie pojęcie o bezpieczeństwie i chciałbyś pomóc w rozwoju programu (non profit) zapraszam do <a href="contact" style="color:green">kontaktu</a>.</p>';
			echo '<h3>Wersja 1.0</h3><span style="text-align:right; color:#fff; font-size:11px">9 kwietnia 2010</span>
			<p>Dziś obroniłem swój projekt na 5 także nie jest źle ;) Narazie z braku czasu projekt jest "taki jaki jest", jak znajde chwilkę czasu to zaczne go rozbudowywać. Jeśli masz jakieś pytania lub propozycje - zapraszam do kontaktu.</p>';
			echo '<h3>Wersja 0.9</h3><span style="text-align:right; color:#fff; font-size:11px">31 stycznia 2010</span>
			<p>Wersja "prawie finalna" jest już na serwerze, pozdrawiam</p>';
			echo '<h3>Wersja 0.5</h3><span style="text-align:right; color:#fff; font-size:11px">17 stycznia 2010</span>
			<p>Witam, druga wersja już bardziej staliblniejsza ujrzała światło dzienne, zapraszam do testowania.</p>';
			echo '<h3>Wersja 0.2 alpha 4</h3><span style="text-align:right; color:#fff; font-size:11px">7 listopada 2009</span>
			<p>Piersza wersja testowa skryptu została dziś przesłana na serwer, zapraszam do testowania i wszystkie błędy lub/i propozyje proszę przesyłać <a href="contact" style="color:green">formularzem kontaktowym</a>.</p>';
			
		break;
		case 'about':
			echo '<h2>O autorze i skrypcie</h2>'; 
			echo '<p style="color:#fff; ">EDIT:<br />
		Skrypt działa już oficjalnie (narazie nie planuje jego rozbudowy z braku czasu).<br />
		
		Jako, że skrypt był częścią mojego projektu inż. mogę pochwalić się, że z projektu uzyskałem <b>5.5</b> a z obrony <b>5.0</b> :)
		</p><p>Nazywam się Łukasz i jestem studentem 4 roku Informatyki .</p>
		<p>Od paru miesięcy projektuje i wdrażam autorski skrypt którego zadaniem jest symulowany atak na aplikacje i sprawdzenie czy jest ona na niego odporna.</p>
		<p>Skrypt który powstaje jest częścią mojej pracy dyplomowej której tematem jest:</p><p class="tytul">&rsquo;Bezpieczeństwo aplikacji internetowych&rsquo;</p>
		<p>Wybrałem taki temat ponieważ interesuje się zabezpieczeniami w aplikacjach www i z doświadczenia wiem, że dużo osób króre zaczynają przygode z programowaniem nie zwraca uwagi na to zagadnienie stając sie podatnymi na ataki.</p>
		<p>Tworząc skrypt chciałbym poprawić wiedzę osób których strony będą skanowane, że nie zawsze napisana przez nich aplikacja jest bezpieczna.</p>
		<p>Aktualnie skrypt przeprowadza symulowany atak pod kontem: (% - oznacza stan wykonania modułu)</p><ol>
		<li><span style="color:green; font-weight: bold; font-size:12px;">100%</span> <b>Cross-site scripting (XSS)</b> – sposób ataku na serwis WWW polegający na osadzeniu w treści atakowanej strony kodu (zazwyczaj JavaScript), który wyświetlony innym użytkownikom może doprowadzić do wykonania przez nich niepożądanych akcji. Skrypt umieszczony w zaatakowanej stronie może obejść niektóre mechanizmy kontroli dostępu do danych użytkownika. - <a href="http://pl.wikipedia.org/wiki/Cross-site_scripting" target="_blank"> więcej </a> </li>
		<li><span style="color:green; font-weight: bold; font-size:12px;">100%</span> <b>SQL Injection </b> – luka w zabezpieczeniach aplikacji internetowych polegająca na nieodpowiednim filtrowaniu lub niedostatecznym typowaniu i późniejszym wykonaniu danych przesyłanych w postaci zapytań SQL do bazy danych. - <a href="http://pl.wikipedia.org/wiki/SQL_injection" target="_blank"> więcej </a> </li>
		<li><span style="color:green; font-weight: bold; font-size:12px;">100%</span> <b>&rsquo;URL Life&rsquo;</b> – sprawdza czy adres URL jest poprawny </li>
		<li><span style="color:green; font-weight: bold; font-size:12px;">100%</span> <b>Remote File Inclusion (RFI) </b> – jest techniką często wykorzystywaną do atakowania stron internetowych z komputera zdalnego. Może umożliwić uruchomienie zainfekowanego kodu PHP na podatnej stronie.</li>
		</ol>
		<p>Skrypt gotowy w <b>100%</b> (w fazie testów)</p>
		
		
		
		';
			
		break;
		case 'rules':
			echo '<h2>Regulamin ver. 0.2</h2><span style="color: #fff; font-size:10px">Ostatnia aktualizacja: 31 styczeń 2010</span>
			<h3>ART. 1 Definicje</h3>
			Użyte w Regulaminie pojęcia oznaczają:<br />
			<b>Regulamin</b> — przedmiotowy dokument normujący prawa i obowiązki Użytkowników.<br />
			<b>Użytkownik</b> — pełnoletnia osoba fizyczna posiadająca pełną zdolność do czynności prawnych, która poprzez akceptację Regulaminu oraz rejestrację uzyskała dostęp do usług oferowanych przez Skrypt.<br />
			<b>Skrypt</b> - aplikacja internetowa testująca zabezpieczenia innych aplikacji internetowych.<br />
			<b>Autor</b> - osoba która napisała Skrypt<br />
			<h3>ART. 2 Postanowienia</h3>
			<ol>
			<li>Adres strony www który zostanie przetestowany jak i ona sama musi być własnością <b>Użytkownika</b>!</li>
			<li>Wszystkie informacje podawane przez skrypt są czysto informacyjne!</li>
			<li><b>Autor</b> NIE może ponieść żadnych konsekwencji związanych z wadliwym lub nieodpowiednim użyciem <b>Skryptu</b> !</li>
			<li>W momencie rejestracji <b>Użytkownik</b> wyraża zgodę na przetwarzanie jego danych osobowych przez autora <b>Skryptu</b>.</li>
			<li>Korzystanie ze <b>Skryptu</b> jest równoznaczne z akceptacją niniejszego <b>Regulaminu</b>.</li>
			<li><b>Autor</b> zastrzega sobie prawo do jednostronnej zmiany postanowień <b>Regulaminu</b> w każdej chwili i bez konieczności uzasadniania przyczyny.</li>
			</ol>';
		break;
		case 'contact':
			echo '<h2>Kontakt</h2>
			<p>Przez formularz poniżej możesz się ze mną skontaktować</p>';
			$mail = new mail();
			$mail->send();
			echo '
			<form method="post" action="contact">
				<p><label>Imię</label><input class="textfield" name="imie" value="'.$vars->show('imie').'" /></p>
				<p><label>E-mail</label><input class="textfield" name="email" value="'.$vars->show('email').'" /></p>
				<p><label for="comment">Treść</label><textarea cols="50" rows="15" name="comment" id="comment">'.$vars->show('comment').'</textarea></p>
				<p><img src="class/captcha.php" alt="CAPTCHA!" /> <span style="vertical-align:10px;">=</span> <input class="textfield_cap" name="cap" value="" /></p>
				<p><label>&nbsp;</label><input class="submit" id="submit" value="wyslij" type="submit" /></p>
			</form>';
		break;
		case 'check':
			echo '<h2>Sprawdź bezpieczeństwo swojej strony</h2>
			Strona na której aktualnie się znajdujesz ma charakter wyłącznie informacyjny, aby dokonać testu proszę przejść na strone projektu klikając <a href="webcheck" style="color:green">TUTAJ</a>
			<p style="color:#fff;">Jeśli skrypt wykaże, że aplikacja jest dziurawa i nie odporna na atak, przydatnym może okazać się <a href="filter.php" stylw="color:red;">TEN</a> skrypt, który filtruje dane, dostępny jest on na licecncji GPU, zapraszam do sciągania i zabezpieczania się ;)</p>
			';
		break;
		case 'subskrypcja':
			echo '<h2>Subskrypcja</h2>
			<p>Możesz zostawić tutaj adres swojej strony która może zostać sprawdzona pod kątem popularnych błędów występujących w aplikacjach www.</p>
			<h3>Pamiętaj, że skrypt jest w ciągłej fazie testowania !</h3>
			';
			$sub = new sub(H_O_ST, U_S_ER, P_A_SS, D_B_AS);
			$sub->sub_add();
		
			echo '
			<form method="post" action="subskrypcja">
					<p><label>Twoje imię</label><input class="textfield" name="imie" value="'.$vars->show('imie').'" /></p>
					<p><label>Adres strony www</label><input class="textfield" name="www" value="'.$vars->show('www').'" /></p>
					<p><label>Twój email </label><input class="textfield" name="email" value="'.$vars->show('email').'" /></p>
					<p><label for="comment">Jakieś uwagi ?</label><textarea cols="30" rows="15" name="comment" id="comment">'.$vars->show('comment').'</textarea></p>
					<p><input type="checkbox" name="ok" value="1" /> Zatwierdzam regulamin i oświadczam, że wprowadzona strona należy do mnie oraz zgadzam sie na przeprowadzenie testu bezpieczenstwa i symulowanego ataku na moj serwis</p>
					<p><img src="class/captcha.php" alt="CAPTCHA!" /> <span style="vertical-align:10px;">=</span> <input class="textfield_cap" name="cap" value="" /></p>
					<p><label for="submit">&nbsp;</label><input class="submit" id="submit" value="wyslij" type="submit" /></p>
			</form>';
		break;
		}
		
		?>
    </div>
    <div id="foot">&copy; 2009/2010 by caraxes</div>
	
    <a href="http://validator.w3.org/check?uri=referer"><img
        src="http://www.w3.org/Icons/valid-xhtml10-blue"
        alt="Valid XHTML 1.0 Transitional" height="31" width="88" /></a>

<a href="http://jigsaw.w3.org/css-validator/check/referer">
    <img style="border:0;width:88px;height:31px"
        src="http://jigsaw.w3.org/css-validator/images/vcss-blue"
        alt="Poprawny CSS!" />
</a>
	
</div>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-13116613-4");
pageTracker._trackPageview();
} catch(err) {}</script>
</body>
</html>
